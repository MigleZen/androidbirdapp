package zenkeviciute.migle.bbd;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import zenkeviciute.migle.bbd.database.CreateDatabase;
import zenkeviciute.migle.bbd.model.CatalogItems;
import zenkeviciute.migle.bbd.util.ChangePhotoDialog;
import zenkeviciute.migle.bbd.util.Init;
import zenkeviciute.migle.bbd.util.UniversalImageLoader;

public class AddCatalogFragment extends Fragment implements ChangePhotoDialog.OnPhotoReceivedListener {

    private static final String TAG = "AddCatalogFragment";

    //private Contact mContact;
    private EditText mName, mDescription, mLocation;
    private CircleImageView mCatalogImage;
    private Toolbar toolbar;
    private String mSelectedImagePath;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.catalog_add_fragment, container, false);
        mName = (EditText) view.findViewById(R.id.etCatalogName);
        mDescription = (EditText) view.findViewById(R.id.etDescription);
        mLocation = (EditText) view.findViewById(R.id.etLocation);
        mCatalogImage = (CircleImageView) view.findViewById(R.id.catalogImage);
        toolbar = (Toolbar) view.findViewById(R.id.editCatalogToolbar);
        Log.d(TAG, "onCreateView: started.");

        mSelectedImagePath = null;

        //load the default images by causing an error
        UniversalImageLoader.setImage(null, mCatalogImage, null, "");

        //set the heading the for the toolbar
        TextView heading = (TextView) view.findViewById(R.id.textCatalogToolbar);
        heading.setText(getString(R.string.add_catalog));

        //required for setting up the toolbar
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);

        //navigation for the backarrow
        ImageView ivBackArrow = (ImageView) view.findViewById(R.id.ivBackArrow);
        ivBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked back arrow.");
                //remove previous fragment from the backstack (therefore navigating back)
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        // initiate the dialog box for choosing an image
        ImageView ivCamera = (ImageView) view.findViewById(R.id.ivCamera);
        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                Make sure all permissions have been verified before opening the dialog
                 */
                for(int i = 0; i < Init.PERMISSIONS.length; i++){
                    String[] permission = {Init.PERMISSIONS[i]};
                    if(((Catalog)getActivity()).checkPermission(permission)){
                        if(i == Init.PERMISSIONS.length - 1){
                            Log.d(TAG, "onClick: opening the 'image selection dialog box'.");
                            ChangePhotoDialog dialog = new ChangePhotoDialog();
                            dialog.show(getFragmentManager(), getString(R.string.change_photo_dialog));
                            dialog.setTargetFragment(AddCatalogFragment.this, 0);
                        }
                    }else{
                        ((Catalog)getActivity()).verifyPermissions(permission);
                    }
                }


            }
        });


        //set onclicklistenre to the 'checkmar' icon for saving a contact
        ImageView confirm = (ImageView) view.findViewById(R.id.ivCheckMark);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: attempting to save new contact.");
                if(checkStringIfNull(mName.getText().toString())){
                    Log.d(TAG, "onClick: saving new contact. " + mName.getText().toString() );

                    CreateDatabase databaseHelper = new CreateDatabase(getActivity());

                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

                    CatalogItems catalog = new CatalogItems(mName.getText().toString(),
                            mDescription.getText().toString(),
                            mLocation.getText().toString(),
                            mSelectedImagePath,
                            date);
                    if(databaseHelper.addCatalog(catalog)){
                        Toast.makeText(getActivity(), "Išsaugota", Toast.LENGTH_SHORT).show();
                        getActivity().getSupportFragmentManager().popBackStack();
                    }else{
                        Toast.makeText(getActivity(), "Klaida", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Įveskite katalogo pavadinimą", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return view;
    }

    private boolean checkStringIfNull(String string){
        if(string.equals("")){
            return false;
        }else{
            return true;
        }
    }

    /**
     * Initialize the onTextChangeListener for formatting the phonenumber
     */
    /**
     * Retrieves the selected image from the bundle (coming from ChangePhotoDialog)
     * @param bitmap
     */
    @Override
    public void getBitmapImage(Bitmap bitmap) {
        Log.d(TAG, "getBitmapImage: got the bitmap: " + bitmap);
        //get the bitmap from 'ChangePhotoDialog'
        if(bitmap != null) {
            //compress the image (if you like)
            ((Catalog)getActivity()).compressBitmap(bitmap, 70);
            mCatalogImage.setImageBitmap(bitmap);
        }
    }

    @Override
    public void getImagePath(String imagePath) {
        Log.d(TAG, "getImagePath: got the image path: " + imagePath);
        File file = new File(imagePath.toString());
        Log.d(TAG, "onActivityResult: images: " + file.getPath());

        mSelectedImagePath = file.getPath();


        if( !mSelectedImagePath.equals("")){
            mSelectedImagePath = mSelectedImagePath.replace(":/", "://");
           // mSelectedImagePath = imagePath;
            UniversalImageLoader.setImage(mSelectedImagePath, mCatalogImage, null, "");
        }
    }
}