package zenkeviciute.migle.bbd;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import zenkeviciute.migle.bbd.model.CatalogItems;
import zenkeviciute.migle.bbd.model.MapObservation;
import zenkeviciute.migle.bbd.model.User;
import zenkeviciute.migle.bbd.util.ChangePhotoDialog;
import zenkeviciute.migle.bbd.util.UniversalImageLoader;
import zenkeviciute.migle.bbd.util.UserClient;

import static android.app.Activity.RESULT_OK;
import static zenkeviciute.migle.bbd.util.Constants.PLACE_PICKER_REQUEST;

public class AddMapObservationFragment extends Fragment implements
        ChangePhotoDialog.OnPhotoReceivedListener,
        ItemDialog.OnInputSelected{

    private static final String TAG = "AddCatalogFragment";
    //private Contact mContact;
    private EditText mDescription, mAmount, mDate;
    public TextView mCatalogName, mName, mLocation;
    private ImageView mCatalogImage;
    private Toolbar toolbar;
    private String mSelectedImagePath, mImage;
    private CatalogItems mCatalog;
    private MapObservation mMapObservation;
    private Double mLatitude, mLongtitude;


    @Override
    public void sendInput(String inputName, String inputImage) {
       // Log.d(TAG, "sendInput: found incoming input: " + input);
        mName.setText(inputName);
        UniversalImageLoader.setImage(inputImage, mCatalogImage, null, "");
    }

    public AddMapObservationFragment(){
        super();
        setArguments(new Bundle());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.map_observation_add_fragment, container, false);
        mName = (TextView) view.findViewById(R.id.etName);
        mLocation = (TextView) view.findViewById(R.id.etLocation);
        mDate = (EditText) view.findViewById(R.id.etDate);
        mDescription = (EditText) view.findViewById(R.id.etDescription);
        mCatalogImage = (ImageView) view.findViewById(R.id.catalogImage);
        mAmount = (EditText) view.findViewById(R.id.etAmount);
        toolbar = (Toolbar) view.findViewById(R.id.editCatalogToolbar);


        mLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                Intent intent;
                try{
                    intent = builder.build(getActivity());
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);
                    // Place place = PlacePicker.getPlace(this, data);
                } catch ( GooglePlayServicesRepairableException e){
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e){
                    e.printStackTrace();
                }

            }
        });

        mName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: opening dialog");

                ItemDialog dialog = new ItemDialog();
                dialog.setTargetFragment(AddMapObservationFragment.this, 1);
                dialog.show(getFragmentManager(), "MyCustomDialog");
            }
        });



        mSelectedImagePath = null;

        //load the default images by causing an error
        UniversalImageLoader.setImage(null, mCatalogImage, null, "");

        //set the heading the for the toolbar
        TextView heading = (TextView) view.findViewById(R.id.textCatalogToolbar);
        heading.setText("Pridėti stebėjimą");

        //required for setting up the toolbar
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);

        //get the contact from the bundle
        mCatalog = getCatalogFromBundle();

        Log.d(TAG, "mCatalog: " + mCatalog);

        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        mDate.setText(date);
        if(mCatalog  != null){
            mName.setText(mCatalog.getName());
        }
        //navigation for the backarrow
        ImageView ivBackArrow = (ImageView) view.findViewById(R.id.ivBackArrow);
        ivBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked back arrow.");
                //remove previous fragment from the backstack (therefore navigating back)
                //getActivity().getSupportFragmentManager().popBackStack();
                Intent intent = new Intent(getActivity(), MapActivity.class);
                startActivity(intent);
            }
        });

        //set onclicklistenre to the 'checkmar' icon for saving a contact
        ImageView confirmNewContact = (ImageView) view.findViewById(R.id.ivCheckMark);
        confirmNewContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: attempting to save new contact.");
                if(checkStringIfNull(mName.getText().toString())){
                    Log.d(TAG, "onClick: saving new contact. " + mName.getText().toString() );

                  //  Toast.makeText(getActivity(), "bandau", Toast.LENGTH_SHORT).show();

                    String name = mName.getText().toString();
                    //String location = mLocation.getText().toString();
                    String amount = mAmount.getText().toString();
                    String description = mDescription.getText().toString();
                    String date = mDate.getText().toString();

                    FirebaseFirestore db = FirebaseFirestore.getInstance();
                    DocumentReference newItemRef = db.collection("Observations")
                            .document();
//                    db.collection("User")
//                            .document(FirebaseAuth.getInstance().getUid());


                    User user = ((UserClient)(getActivity().getApplicationContext())).getUser();
                    Log.d("AddMap ", "user " + user.getUser_id() + " " + user.getUsername() );

                    MapObservation map = new MapObservation();

                    GeoPoint geoPoint = new GeoPoint(mLatitude, mLongtitude);


                    map.setName(name);
                    map.setAmount(amount);
                    map.setDescription(description);
                    map.setObservation_id(newItemRef.getId());
                    map.setGeo_point(geoPoint);
                    map.setDate(date);
                    map.setImage(mImage);
                    map.setUser(user);


                    newItemRef.set(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Log.d(TAG, "succesful2: saving new contact. " + mName.getText().toString() );
                                Toast.makeText(getActivity(), "Sėkmingai įkelta", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getActivity(), MapActivity.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(getActivity(), "Klaida", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    }else{
                        Toast.makeText(getActivity(), "Klaida", Toast.LENGTH_SHORT).show();
                    }


            }
        });

        return view;
    }

    /**
     * Retrieves the selected contact from the bundle (coming from MainActivity)
     * @return
     */
    private CatalogItems getCatalogFromBundle(){
        Log.d(TAG, "getContactFromBundle: arguments AddObservationFragment: " + getArguments());

        Bundle bundle = this.getArguments();
        if(bundle != null){
            return bundle.getParcelable(getString(R.string.catalog));
        }else{
            return null;
        }
    }

    private boolean checkStringIfNull(String string){
        if(string.equals("")){
            return false;
        }else{
            return true;
        }
    }

    /**
     * Initialize the onTextChangeListener for formatting the phonenumber
     */

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.catalog_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menuitem_delete:
                Log.d(TAG, "onOptionsItemSelected: deleting contact.");
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Retrieves the selected image from the bundle (coming from ChangePhotoDialog)
     * @param bitmap
     */
    @Override
    public void getBitmapImage(Bitmap bitmap) {
        Log.d(TAG, "getBitmapImage: got the bitmap: " + bitmap);
        //get the bitmap from 'ChangePhotoDialog'
        if(bitmap != null) {
            //compress the image (if you like)
            ((Catalog)getActivity()).compressBitmap(bitmap, 70);
            mCatalogImage.setImageBitmap(bitmap);
        }
    }

    @Override
    public void getImagePath(String imagePath) {
        Log.d(TAG, "getImagePath: got the image path: " + imagePath);

        if( !imagePath.equals("")){
            imagePath = imagePath.replace(":/", "://");
            mSelectedImagePath = imagePath;
            UniversalImageLoader.setImage(imagePath, mCatalogImage, null, "");
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode==PLACE_PICKER_REQUEST){
            if(resultCode==RESULT_OK){
                Place place = PlacePicker.getPlace(getActivity(), data);
                mLatitude = place.getLatLng().latitude;
                mLongtitude = place.getLatLng().longitude;
                mLocation.setText(place.getAddress().toString());

                Log.d("TAG", "place address" + place.getAddress().toString());
                Log.d("TAG", "place name" + place.getName().toString());
            }

        }
    }
}