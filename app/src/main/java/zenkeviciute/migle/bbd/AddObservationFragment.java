package zenkeviciute.migle.bbd;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import zenkeviciute.migle.bbd.database.ObservationDatabase;
import zenkeviciute.migle.bbd.model.CatalogItems;
import zenkeviciute.migle.bbd.model.ObservationItems;
import zenkeviciute.migle.bbd.util.ChangePhotoDialog;
import zenkeviciute.migle.bbd.util.Init;
import zenkeviciute.migle.bbd.util.UniversalImageLoader;

public class AddObservationFragment extends Fragment implements
        ChangePhotoDialog.OnPhotoReceivedListener,
        ItemDialog.OnInputSelected{

    private static final String TAG = "AddCatalogFragment";

    public AddObservationFragment(){
        super();
        setArguments(new Bundle());
    }

    //private Contact mContact;
    private EditText mDescription, mAmount, mDate, mLocation;
    public TextView mCatalogName, mName;
    private CircleImageView mCatalogImage;
    private Toolbar toolbar;
    private String mSelectedImagePath, mImage;
    private CatalogItems mCatalog;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.observation_add_fragment, container, false);
        mCatalogName = (TextView) view.findViewById(R.id.etCatalogName);
        mLocation = (EditText) view.findViewById(R.id.etLocation);
        mName = (TextView) view.findViewById(R.id.etName);
        mDescription = (EditText) view.findViewById(R.id.etDescription);
        mCatalogImage = (CircleImageView) view.findViewById(R.id.catalogImage);
        mAmount = (EditText) view.findViewById(R.id.etAmount);
        mDate = (EditText) view.findViewById(R.id.etDate);
        toolbar = (Toolbar) view.findViewById(R.id.editCatalogToolbar);

        mName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: opening dialog");

                ItemDialog dialog = new ItemDialog();
                dialog.setTargetFragment(AddObservationFragment.this, 1);
                dialog.show(getFragmentManager(), "MyCustomDialog");
            }
        });

        //mSelectedImagePath = null;

        //load the default images by causing an error
        UniversalImageLoader.setImage(null, mCatalogImage, null, "");

        //set the heading the for the toolbar
        TextView heading = (TextView) view.findViewById(R.id.textCatalogToolbar);
        heading.setText(getString(R.string.add_observation));

        //required for setting up the toolbar
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);

        //get the contact from the bundle
        mCatalog = getCatalogFromBundle();

        Log.d(TAG, "mCatalog: " + mCatalog);

        if(mCatalog  != null){
            init();
        }
        //navigation for the backarrow
        ImageView ivBackArrow = (ImageView) view.findViewById(R.id.ivBackArrow);
        ivBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked back arrow.");
                //remove previous fragment from the backstack (therefore navigating back)
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        // initiate the dialog box for choosing an image
        ImageView ivCamera = (ImageView) view.findViewById(R.id.ivCamera);
        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                Make sure all permissions have been verified before opening the dialog
                 */
                for(int i = 0; i < Init.PERMISSIONS.length; i++){
                    String[] permission = {Init.PERMISSIONS[i]};
                    if(((Catalog)getActivity()).checkPermission(permission)){
                        if(i == Init.PERMISSIONS.length - 1){
                            Log.d(TAG, "onClick: opening the 'image selection dialog box'.");
                            ChangePhotoDialog dialog = new ChangePhotoDialog();
                            dialog.show(getFragmentManager(), getString(R.string.change_photo_dialog));
                            dialog.setTargetFragment(AddObservationFragment.this, 0);
                        }
                    }else{
                        ((Catalog)getActivity()).verifyPermissions(permission);
                    }
                }


            }
        });


        //set onclicklistenre to the 'checkmar' icon for saving a contact
        ImageView confirmNewContact = (ImageView) view.findViewById(R.id.ivCheckMark);
        confirmNewContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: attempting to save new contact.");
                if(!mName.getText().toString().matches("(Pasirinkite).*")){
                    Log.d(TAG, "onClick: saving new contact. " + mName.getText().toString() );

                    ObservationDatabase databaseHelper = new ObservationDatabase(getActivity());
                    ObservationItems catalog = new ObservationItems(
                            mCatalog.getName(),
                            mName.getText().toString(),
                            mLocation.getText().toString(),
                            mAmount.getText().toString(),
                            mDescription.getText().toString(),
                            mSelectedImagePath,
                            mDate.getText().toString());
                    if(databaseHelper.addCatalog(catalog)){
                        Toast.makeText(getActivity(), "Įrašas išsaugotas", Toast.LENGTH_SHORT).show();
                        getActivity().getSupportFragmentManager().popBackStack();
                    }else{
                        Toast.makeText(getActivity(), "Klaida", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Pasirinkite paukštį", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    private void init(){
        //mCatalogName.setText(mCatalog.getName());
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        mDate.setText(date);
    }

    /**
     * Retrieves the selected contact from the bundle (coming from MainActivity)
     * @return
     */
    private CatalogItems getCatalogFromBundle(){
        Log.d(TAG, "getContactFromBundle: arguments AddObservationFragment: " + getArguments());

        Bundle bundle = this.getArguments();
        if(bundle != null){
            return bundle.getParcelable(getString(R.string.catalog));
        }else{
            return null;
        }
    }

    private boolean checkStringIfNull(String string){
        if(string.equals("")){
            return false;
        }else{
            return true;
        }
    }

    /**
     * Retrieves the selected image from the bundle (coming from ChangePhotoDialog)
     * @param bitmap
     */
    @Override
    public void getBitmapImage(Bitmap bitmap) {
        Log.d(TAG, "getBitmapImage: got the bitmap: " + bitmap);
        //get the bitmap from 'ChangePhotoDialog'
        if(bitmap != null) {
            //compress the image (if you like)
            ((Catalog)getActivity()).compressBitmap(bitmap, 70);
            mCatalogImage.setImageBitmap(bitmap);
        }
    }

    @Override
    public void getImagePath(String imagePath) {
        Log.d(TAG, "getImagePath: got the image path: " + imagePath);
        if( !imagePath.equals("")){
            imagePath = imagePath.replace(":/", "://");
            mSelectedImagePath = imagePath;
            UniversalImageLoader.setImage(imagePath, mCatalogImage, null, "");
        }
    }

    @Override
    public void sendInput(String inputName, String inputImage) {
        mName.setText(inputName);
        mSelectedImagePath = inputImage;
        UniversalImageLoader.setImage(inputImage, mCatalogImage, null, "");
    }

}