package zenkeviciute.migle.bbd;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import zenkeviciute.migle.bbd.adapter.ObservationListAdapter;
import zenkeviciute.migle.bbd.database.CreateDatabase;
import zenkeviciute.migle.bbd.database.ObservationDatabase;
import zenkeviciute.migle.bbd.model.CatalogItems;
import zenkeviciute.migle.bbd.model.ObservationItems;
import zenkeviciute.migle.bbd.util.UniversalImageLoader;

public class CatalogItemFragment extends Fragment{
    private static final String TAG = "ContactFragment";

    public interface OnEditCatalogListener{
        public void onEditCatalogSelected(CatalogItems catalog);
    }

    OnEditCatalogListener mOnEditCatalogListener;


    //This will evade the nullpointer exception whena adding to a new bundle from MainActivity
    public CatalogItemFragment(){
        super();
        setArguments(new Bundle());
    }

    private Toolbar toolbar;
    private CatalogItems mCatalog;
    private TextView mCatalogName, mCatalogLocation, mCatalogDescription, mToolbar;
    private CircleImageView mCatalogImage;
    private ListView mList;
    private ObservationListAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.catalog_item_fragment, container, false);
        toolbar = (Toolbar) view.findViewById(R.id.catalogToolbar);
        mCatalogName = (TextView) view.findViewById(R.id.catalogName);
        mCatalogLocation = (TextView) view.findViewById(R.id.cataloglocation);
        mCatalogDescription = (TextView) view.findViewById(R.id.catalogDescription);
        mCatalogImage = (CircleImageView) view.findViewById(R.id.catalogImage);
        mList = (ListView) view.findViewById(R.id.observationList);
        mToolbar = (TextView) view.findViewById(R.id.textCatalogToolbar);
        Log.d(TAG, "onCreateView: started.");
        mCatalog = getCatalogFromBundle();

        //required for setting up the toolbar
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);

        init();

        //navigation for the backarrow
        ImageView ivBackArrow = (ImageView) view.findViewById(R.id.ivBackArrow);
        ivBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked back arrow.");
                //remove previous fragment from the backstack (therefore navigating back)
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        // navigate to the edit contact fragment to edit the contact selected
        ImageView ivEdit = (ImageView) view.findViewById(R.id.ivEdit);
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked the edit icon.");
                mOnEditCatalogListener.onEditCatalogSelected(mCatalog);
            }
        });

        // navigate to add contacts fragment
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.addObservation);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked fab.");
                //mOnAddCatalog.onAddCatalog();
                AddObservationFragment fragment = new AddObservationFragment();
                Bundle args = new Bundle();
                args.putParcelable(getString(R.string.catalog), mCatalog);
                fragment.setArguments(args);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, fragment);
                transaction.addToBackStack(getString(R.string.add_catalog_fragment));
                transaction.commit();
            }
        });

        return view;
    }

    private void init(){
        mToolbar.setText("Stebėjimai");
        mCatalogName.setText(mCatalog.getName());
        mCatalogLocation.setText(mCatalog.getLocation());
        mCatalogDescription.setText(mCatalog.getDescription());
        UniversalImageLoader.setImage(mCatalog.getLocationImage(), mCatalogImage, null, "");

        listAdapter();
    }

    public void listAdapter(){
        final ArrayList<ObservationItems> catalog = new ArrayList<>();
        ObservationDatabase databaseHelper = new ObservationDatabase(getActivity());
        Cursor cursor = databaseHelper.getItemsByCatalog(mCatalog.getName());
        //iterate through all the rows contained in the database
        if (!cursor.moveToNext()) {
            Toast.makeText(getActivity(), "Nėra įvestų stebėjimų", Toast.LENGTH_SHORT).show();
        }
        cursor.moveToPosition(-1);
        while (cursor.moveToNext()) {
            catalog.add(new ObservationItems(
                    cursor.getString(1),//name
                    cursor.getString(2),//location
                    cursor.getString(3),//description
                    cursor.getString(4),//picture
                    cursor.getString(5),//picture
                    cursor.getString(6),//picture
                    cursor.getString(7)//picture
            ));
        }
        cursor.close();

        Log.d(TAG, "catalog items " + catalog.toString());

        adapter = new ObservationListAdapter(getActivity(), R.layout.catalog_item, catalog, "");
        mList.setAdapter(adapter);

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onClick: navigating to " + getString(R.string.catalog_fragment));

                ObservationItemFragment fragment = new ObservationItemFragment();
                Bundle args = new Bundle();
                args.putParcelable(getString(R.string.catalog), catalog.get(position));
                fragment.setArguments(args);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, fragment);
                transaction.addToBackStack(getString(R.string.add_catalog_fragment));
                transaction.commit();

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        CreateDatabase databaseHelper = new CreateDatabase(getActivity());
        Cursor cursor  = databaseHelper.getCatalogID(mCatalog);

        int contactID = -1;
        while(cursor.moveToNext()){
            contactID = cursor.getInt(0);
        }
        if(contactID > -1){ // If the contact doesn't still exists then anvigate back by popping the stack
            init();
        }else{
            this.getArguments().clear(); //optional clear arguments but not necessary
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.catalog_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menuitem_delete:
                CreateDatabase databaseHelper = new CreateDatabase(getActivity());
                Cursor cursor = databaseHelper.getCatalogID(mCatalog);

                int contactID = -1;
                while(cursor.moveToNext()){
                    contactID = cursor.getInt(0);
                }
                if(contactID > -1){
                    if(databaseHelper.deleteCatalog(contactID) > 0){
                        Toast.makeText(getActivity(), "Ištrinta", Toast.LENGTH_SHORT).show();

                        //clear the arguments ont he current bundle since the contact is deleted
                        this.getArguments().clear();

                        //remove previous fragemnt from the backstack (therefore navigating back)
                        getActivity().getSupportFragmentManager().popBackStack();
                    }
                    else{
                        Toast.makeText(getActivity(), "Klaida", Toast.LENGTH_SHORT).show();
                    }
                }
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Retrieves the selected contact from the bundle (coming from MainActivity)
     * @return
     */
    private CatalogItems getCatalogFromBundle(){
        Log.d(TAG, "getContactFromBundle: arguments CatalogItemFragment: " + getArguments());

        Bundle bundle = this.getArguments();
        if(bundle != null){
            return bundle.getParcelable(getString(R.string.catalog));
        }else{
            return null;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            mOnEditCatalogListener = (OnEditCatalogListener) getActivity();
        }catch (ClassCastException e){
            Log.e(TAG, "onAttach: ClassCastException: " + e.getMessage() );
        }
    }
}
