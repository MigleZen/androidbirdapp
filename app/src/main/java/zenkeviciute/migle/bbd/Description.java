package zenkeviciute.migle.bbd;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import zenkeviciute.migle.bbd.adapter.SectionsPageAdapter;

public class Description extends AppCompatActivity {
    private static final String TAG = "Description";

    private SectionsPageAdapter mSectionsPageAdapter;

    private ViewPager mViewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.description);

        getIncomingIntent();

        mSectionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new DescriptionFragment(), "TAB1");
        adapter.addFragment(new DescriptionMainFragment(), "TAB2");
        viewPager.setAdapter(adapter);
    }

    public void getIncomingIntent(){
        if(getIntent().hasExtra("name")){

            String itemName = getIntent().getStringExtra("name");
            // String itemDescription = getIntent().getStringExtra("description");

            //TextView name = findViewById(R.id.image_description);
            //name.setText(itemName);

        }
    }
}
