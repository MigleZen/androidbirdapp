package zenkeviciute.migle.bbd;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import zenkeviciute.migle.bbd.model.Items;

public class DescriptionFragment extends Fragment {
    private static final String TAG = "Tab1Fragment";

    private TextView mDescription;
    private Items mCatalog;

    private String toolbarName;

    private DatabaseReference mRef;
    private FirebaseDatabase mFirebaseDatabase;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.description_fragment,container,false);
        mDescription = (TextView) view.findViewById(R.id.description);

        toolbarName = getActivity().getIntent().getStringExtra("name");
        Log.d(TAG, "info " +toolbarName);

        getData();
        init();

        return view;
    }

    private void init(){
//        mDescription.setText(mCatalog.getDescription());
    }

    private void getData() {

        final ArrayList<Items> catalog = new ArrayList<>();
        final Items items = new Items();


        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mRef = mFirebaseDatabase.getReference("items");

        Query query = mRef.orderByChild("name").equalTo(toolbarName);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    catalog.add((Items) ds.getValue(Items.class));

                    String name = ds.getValue(Items.class).getName();
                    String family = ds.getValue(Items.class).getFamily();

                    Log.d(TAG, "showData: family: " + ds.getValue(Items.class).getFamily());
                    Log.d(TAG, "showData: name: " + ds.getValue(Items.class).getName());
                    //Log.d(TAG, "showData: catalog: " + catalog.toString());
                    // Log.d(TAG, "showData: catalog family: " + catalog.get(0));
                    mDescription.setText(ds.getValue(Items.class).getDescription());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
