package zenkeviciute.migle.bbd;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import zenkeviciute.migle.bbd.model.Items;
import zenkeviciute.migle.bbd.util.UniversalImageLoader;

public class DescriptionMainFragment extends Fragment {
    private static final String TAG = "Tab1Fragment";

    private Button btnTEST;
    private TextView mOrder, mFamily, mRedBook, mObsData, mLname;
    private Items mCatalog;
    private ImageView mImage;

    private String toolbarName;

    private DatabaseReference mRef;
    private FirebaseDatabase mFirebaseDatabase;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.description_main_fragment,container,false);
        mOrder = (TextView) view.findViewById(R.id.order);
        mFamily = (TextView) view.findViewById(R.id.family);
        mRedBook = (TextView) view.findViewById(R.id.redBook);
        mObsData = (TextView) view.findViewById(R.id.obsData);
        mImage = (ImageView) view.findViewById(R.id.image);
        mLname = (TextView) view.findViewById(R.id.lname);

        toolbarName = getActivity().getIntent().getStringExtra("name");
        Log.d(TAG, "info " +toolbarName);

        getData();

        return view;
    }


    private void getData() {

        final ArrayList<Items> catalog = new ArrayList<>();
        final Items items = new Items();

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mRef = mFirebaseDatabase.getReference("items");
        //mRef.orderByChild("name").equalTo(toolbarName);
        Log.d("TAG", "toolbar" + toolbarName);

        Query query = mRef.orderByChild("name").equalTo(toolbarName);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot ds : dataSnapshot.getChildren()){

                    catalog.add((Items) ds.getValue(Items.class) );

                    String name = ds.getValue(Items.class).getName();
                    String family = ds.getValue(Items.class).getFamily();

                    Log.d(TAG, "showData: family: " + ds.getValue(Items.class).getFamily());
                    Log.d(TAG, "showData cat: name: " + ds.getValue(Items.class).getName());
                    //Log.d(TAG, "showData: catalog: " + catalog.toString());
                    // Log.d(TAG, "showData: catalog family: " + catalog.get(0));
                    mOrder.setText(ds.getValue(Items.class).getOrder());
                    mFamily.setText(ds.getValue(Items.class).getFamily());
                    mLname.setText(ds.getValue(Items.class).getLname());

                    Log.d("TAG", "redbook " + ds.getValue(Items.class).getRedbook());

                    if(ds.getValue(Items.class).getRedbook().equals("1")){
                        mRedBook.setText("Rūšis įtraukta į Raudonąją knygą");
                    }
                    if(ds.getValue(Items.class).getObsData().equals("1")){
                        mObsData.setText("Renkami rūšies stebėjimų duomenys");
                    }

                    String imagePath  = ds.getValue(Items.class).getImage();
                    UniversalImageLoader.setImage(imagePath, mImage, null, "");
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
