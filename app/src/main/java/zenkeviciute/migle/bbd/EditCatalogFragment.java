package zenkeviciute.migle.bbd;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import de.hdodenhof.circleimageview.CircleImageView;
import zenkeviciute.migle.bbd.database.CreateDatabase;
import zenkeviciute.migle.bbd.model.CatalogItems;
import zenkeviciute.migle.bbd.util.ChangePhotoDialog;
import zenkeviciute.migle.bbd.util.Init;
import zenkeviciute.migle.bbd.util.UniversalImageLoader;

public class EditCatalogFragment extends Fragment implements ChangePhotoDialog.OnPhotoReceivedListener{
    private static final String TAG = "EditContactFragment";

    //This will evade the nullpointer exception whena adding to a new bundle from MainActivity
    public EditCatalogFragment(){
        super();
        setArguments(new Bundle());
    }

    private CatalogItems mCatalog;
    private EditText mName, mLocation, mDescription;
    private CircleImageView mCatalogImage;
    private Toolbar toolbar;
    private String mSelectedImagePath;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.catalog_edit_fragment, container, false);
        mName = (EditText) view.findViewById(R.id.etCatalogName);
        mLocation = (EditText) view.findViewById(R.id.etLocation);
        mDescription = (EditText) view.findViewById(R.id.etDescription);
        mCatalogImage = (CircleImageView) view.findViewById(R.id.catalogImage);
        toolbar = (Toolbar) view.findViewById(R.id.editCatalogToolbar);
        Log.d(TAG, "onCreateView: started.");
        mSelectedImagePath = null;
        //set the heading the for the toolbar
        TextView heading = (TextView) view.findViewById(R.id.textCatalogToolbar);
        heading.setText(getString(R.string.edit_catalog));

        //required for setting up the toolbar
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);

        //get the contact from the bundle
        mCatalog = getCatalogFromBundle();

        if(mCatalog != null){
            init();
        }

        //navigation for the backarrow
        ImageView ivBackArrow = (ImageView) view.findViewById(R.id.ivBackArrow);
        ivBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked back arrow.");
                //remove previous fragment from the backstack (therefore navigating back)
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        // save changes to the contact
        ImageView ivCheckMark = (ImageView) view.findViewById(R.id.ivCheckMark);
        ivCheckMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: saving the edited contact.");
                //execute the save method for the database

                if (checkStringIfNull(mName.getText().toString())) {
                    Log.d(TAG, "onClick: saving changes to the contact: " + mName.getText().toString());

                    //get the database helper and save the contact
                    CreateDatabase databaseHelper = new CreateDatabase(getActivity());
                    Cursor cursor = databaseHelper.getCatalogID(mCatalog);

                    int catalogID = -1;
                    while (cursor.moveToNext()) {
                        catalogID = cursor.getInt(0);
                    }
                    if (catalogID > -1) {
                        if (mSelectedImagePath != null) {
                            mCatalog.setLocationImage(mSelectedImagePath);
                        }
                        mCatalog.setName(mName.getText().toString());
                        mCatalog.setLocation(mLocation.getText().toString());
                        mCatalog.setDescription(mDescription.getText().toString());

                        databaseHelper.updateCatalog(mCatalog, catalogID);
                        Toast.makeText(getActivity(), "Katalogas atnaujintas", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "Klaida", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Įveskite katalogo pavadinimą", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // initiate the dialog box for choosing an image
        ImageView ivCamera = (ImageView) view.findViewById(R.id.ivCamera);
        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                Make sure all permissions have been verified before opening the dialog
                 */
                for( int i = 0; i < Init.PERMISSIONS.length; i++){
                    String[] permission = {Init.PERMISSIONS[i]};
                    if(((Catalog)getActivity()).checkPermission(permission)){
                        if(i == Init.PERMISSIONS.length - 1){
                            Log.d(TAG, "onClick: opening the 'image selection dialog box'.");
                            ChangePhotoDialog dialog = new ChangePhotoDialog();
                            dialog.show(getFragmentManager(), getString(R.string.change_photo_dialog));
                            dialog.setTargetFragment(EditCatalogFragment.this, 0);
                        }
                    }else{
                        ((Catalog)getActivity()).verifyPermissions(permission);
                    }
                }


            }
        });

        return view;
    }
    private boolean checkStringIfNull(String string){
        if(string.equals("")){
            return false;
        }else{
            return true;
        }
    }

    private void init(){
        mName.setText(mCatalog.getName());
        mLocation.setText(mCatalog.getLocation());
        mDescription.setText(mCatalog.getDescription());
        UniversalImageLoader.setImage(mCatalog.getLocationImage(), mCatalogImage, null, "");
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.catalog_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menuitem_delete:
                CreateDatabase databaseHelper = new CreateDatabase(getActivity());
                Cursor cursor = databaseHelper.getCatalogID(mCatalog);

                int contactID = -1;
                while(cursor.moveToNext()){
                    contactID = cursor.getInt(0);
                }
                if(contactID > -1){
                    if(databaseHelper.deleteCatalog(contactID) > 0){
                        Toast.makeText(getActivity(), "Pašalinta", Toast.LENGTH_SHORT).show();

                        //clear the arguments ont he current bundle since the contact is deleted
                        this.getArguments().clear();

                        //remove previous fragemnt from the backstack (therefore navigating back)
                        getActivity().getSupportFragmentManager().popBackStack();
                    }
                    else{
                        Toast.makeText(getActivity(), "Klaida", Toast.LENGTH_SHORT).show();
                    }
                }
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Retrieves the selected contact from the bundle (coming from MainActivity)
     * @return
     */
    private CatalogItems getCatalogFromBundle(){
        Log.d(TAG, "getContactFromBundle: arguments: " + getArguments());

        Bundle bundle = this.getArguments();
        if(bundle != null){
            return bundle.getParcelable(getString(R.string.catalog));
        }else{
            return null;
        }
    }

    /**
     * Retrieves the selected image from the bundle (coming from ChangePhotoDialog)
     * @param bitmap
     */
    @Override
    public void getBitmapImage(Bitmap bitmap) {
        Log.d(TAG, "getBitmapImage: got the bitmap: " + bitmap);
        //get the bitmap from 'ChangePhotoDialog'
        if(bitmap != null) {
            //compress the image (if you like)
            ((Catalog)getActivity()).compressBitmap(bitmap, 70);
            mCatalogImage.setImageBitmap(bitmap);
        }
    }

    @Override
    public void getImagePath(String imagePath) {
        Log.d(TAG, "getImagePath: got the image path: " + imagePath);

        if( !imagePath.equals("")){
            imagePath = imagePath.replace(":/", "://");
            mSelectedImagePath = imagePath;
            UniversalImageLoader.setImage(imagePath, mCatalogImage, null, "");
        }
    }
}

