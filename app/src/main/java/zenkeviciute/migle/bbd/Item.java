package zenkeviciute.migle.bbd;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import zenkeviciute.migle.bbd.R;
import zenkeviciute.migle.bbd.model.Items;
import zenkeviciute.migle.bbd.ui.LoginActivity;
import zenkeviciute.migle.bbd.util.BottomNavigationViewHelper;
import zenkeviciute.migle.bbd.util.UniversalImageLoader;

public class Item extends AppCompatActivity implements
        ItemFragment.OnCatalogSelectedListener{

    private static final String TAG = "MainActivity";

    private static final int REQUEST_CODE = 1;
    private BottomNavigationViewEx bottomNavigationViewEx;

    @Override
    public void OnCatalogSelected(Items catalog) {
        Log.d(TAG, "OnContactSelected: contact selected from "
                + getString(R.string.catalog_item)
                + " " + catalog.getName());

        Log.d(TAG, "katalogo seima "
                + " " + catalog.getFamily());

        ViewItemFragment fragment = new ViewItemFragment();
        Bundle args = new Bundle();
        args.putParcelable(getString(R.string.catalog), catalog);
        fragment.setArguments(args);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack("ItemFragment");
        transaction.commit();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: started.");
        bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        bottomNavigationViewEx.setSelectedItemId(R.id.nav_items);
        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_chat:
                        Intent intent1 = new Intent(Item.this, LoginActivity.class);
                        intent1.putExtra("redirect", "chat");
                        startActivity(intent1);
                        break;
                    case R.id.nav_catalog:
                        Intent intent2 = new Intent(Item.this, Catalog.class);
                        startActivity(intent2);
                        break;
                    case R.id.nav_items:
                        item.setChecked(true);
                        break;
                    case R.id.nav_map:
                        Intent intent4 = new Intent(Item.this, LoginActivity.class);
                        intent4.putExtra("redirect", "map");
                        startActivity(intent4);
                        break;
                }
                return false;
            }
        });

        initImageLoader();
        init();
    }

    /**
     * initialize the first fragment (ViewContactsFragment)
     */
    private void init(){
        ItemFragment fragment = new ItemFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();
    }

    private void initImageLoader(){
        UniversalImageLoader universalImageLoader = new UniversalImageLoader(Item.this);
        ImageLoader.getInstance().init(universalImageLoader.getConfig());
    }

    /**
     * Compress a bitmap by the @param "quality"
     * Quality can be anywhere from 1-100 : 100 being the highest quality.
     * @param bitmap
     * @param quality
     * @return
     */
    public Bitmap compressBitmap(Bitmap bitmap, int quality){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, stream);
        return bitmap;
    }

    /**
     * Generalized method for asking permission. Can pass any array of permissions
     * @param permissions
     */
    public void verifyPermissions(String[] permissions){
        Log.d(TAG, "verifyPermissions: asking user for permissions.");
        ActivityCompat.requestPermissions(
                Item.this,
                permissions,
                REQUEST_CODE
        );
    }

    /**
     * Checks to see if permission was granted for the passed parameters
     * ONLY ONE PERMISSION MAYT BE CHECKED AT A TIME
     * @param permission
     * @return
     */
    public boolean checkPermission(String[] permission){
        Log.d(TAG, "checkPermission: checking permissions for:" + permission[0]);

        int permissionRequest = ActivityCompat.checkSelfPermission(
                Item.this,
                permission[0]);

        if(permissionRequest != PackageManager.PERMISSION_GRANTED){
            Log.d(TAG, "checkPermission: \n Permissions was not granted for: " + permission[0]);
            return false;
        }else{
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: requestCode: " + requestCode);

        switch(requestCode){
            case REQUEST_CODE:
                for(int i = 0; i < permissions.length; i++){
                    if(grantResults[i] == PackageManager.PERMISSION_GRANTED){
                        Log.d(TAG, "onRequestPermissionsResult: UserActivity has allowed permission to access: " + permissions[i]);
                    }else{
                        break;
                    }
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        bottomNavigationViewEx.setSelectedItemId(R.id.nav_items);

    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(Item.this, Item.class);
        startActivity(intent);
    }


}