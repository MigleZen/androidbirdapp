package zenkeviciute.migle.bbd;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import zenkeviciute.migle.bbd.adapter.ItemListAdapter;
import zenkeviciute.migle.bbd.model.Items;

public class ItemFragment extends Fragment {
    private static final String TAG = "ItemFragment";

    public interface OnCatalogSelectedListener{
        public void OnCatalogSelected(Items items);
    }
    OnCatalogSelectedListener mCatalogListener;

    //variables and widgets
    private static final int STANDARD_APPBAR = 0;
    private static final int SEARCH_APPBAR = 1;
    private int mAppBarState;

    private AppBarLayout viewCatalogBar, searchBar;
    private ItemListAdapter adapter;
    private ListView catalogList;
    private EditText mSearchItems;
    private TextView mSearch;
    private ProgressBar mProgressBar;

    private DatabaseReference mRef;
    private FirebaseDatabase mFirebaseDatabase;

    private String mFamily = "0", mOrder = "0", mRedBook = "0", mObsData = "0", mLname = "0", mName = "1";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_fragment, container, false);
        viewCatalogBar = (AppBarLayout) view.findViewById(R.id.item_toolbar);
        searchBar = (AppBarLayout) view.findViewById(R.id.search_toolbar);
        catalogList = (ListView) view.findViewById(R.id.catalogList);
        mSearchItems = (EditText) view.findViewById(R.id.etSearchCatalog);
        mSearch = (TextView) view.findViewById(R.id.search);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);

        boolean internet = false;
        try {
            internet = isConnected();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(internet){
            Toast.makeText(getActivity(), "Duomenys atnaujinami. Prašome palaukti", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Įjunkite interneto ryšį, norėdami atnaujinti duomenis.", Toast.LENGTH_SHORT).show();
        }

        mSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
                View searchView = getLayoutInflater().inflate(R.layout.search_dialog, null);
                mBuilder.setTitle("Išplėstinė paieška");
                final Spinner singleItems = (Spinner) searchView.findViewById(R.id.singleItems);
                final CheckBox redBook = (CheckBox) searchView.findViewById(R.id.redBook);
                final CheckBox obsData = (CheckBox) searchView.findViewById(R.id.obsData);

                ArrayAdapter<String> searchAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item,
                    getResources().getStringArray(R.array.search_single_items));
                searchAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                singleItems.setAdapter(searchAdapter);

                if(mRedBook.equals("1")){
                    redBook.setChecked(true);
                }
                if(mObsData.equals("1")){
                    obsData.setChecked(true);
                }
                if(mFamily.equals("1")){
                    singleItems.setSelection(3);
                }
                if(mOrder.equals("1")){
                    singleItems.setSelection(2);
                }
                if(mName.equals("1")){
                    singleItems.setSelection(0);
                }
                if(mLname.equals("1")){
                    singleItems.setSelection(1);
                }

                mBuilder.setPositiveButton("Patvirtinti", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        if(!singleItems.getSelectedItem().toString().equalsIgnoreCase("-")) {
                            Log.d("TAG", "items" + singleItems.getSelectedItem().toString());
                            String item = singleItems.getSelectedItem().toString();
                            Log.d("TAG", "itemas" + item);

                            if (item.equals("Ieškoti pagal būrį")) {
                                mOrder = "1";
                                mName = "0";
                                mFamily = "0";
                                mLname = "0";
                            } else if (item.equals("Ieškoti pagal šeimą")) {
                                mFamily = "1";
                                mName = "0";
                                mOrder = "0";
                                mLname = "0";
                            } else if (item.equals("Ieškoti pagal lotynišką pavadinimą")) {
                                mLname = "1";
                                mName = "0";
                                mOrder = "0";
                                mFamily = "0";
                            } else if (item.equals("Ieškoti pagal pavadinimą")) {
                                mName = "1";
                                mOrder = "0";
                                mFamily = "0";
                                mLname = "0";
                            }
                        }
                            Log.d("mOrder", " " + mOrder);
                        Log.d("mLname", " " + mLname);
                        Log.d("mName", " " + mName);
                            Log.d("mFamily", " " + mFamily);

                           if(redBook.isChecked()){
                                mRedBook = "1";
                           } else {
                               mRedBook = "0";
                           }

                           if(obsData.isChecked()){
                                mObsData = "1";
                           } else {
                               mObsData = "0";
                           }

                        Log.d("mRedBook", " " + mRedBook);
                        Log.d("obsData", " " + mObsData);
                        setupContactsList();
                    }
                });

                mBuilder.setNegativeButton("Atšaukti", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                mBuilder.setView(searchView);
                AlertDialog mDialog = mBuilder.create();
                mDialog.show();
            }
        });

        Log.d(TAG, "onCreateView: started.");

        setAppBarState(STANDARD_APPBAR);

        setupContactsList();

        ImageView ivSearch = (ImageView) view.findViewById(R.id.ivSearchIcon);
        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked search icon.");
                toggleToolBarState();
            }
        });

        ImageView ivBackArrow = (ImageView) view.findViewById(R.id.ivBackArrow);
        ivBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked back arrow.");
                toggleToolBarState();
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            mCatalogListener = (OnCatalogSelectedListener) getActivity();
        }catch (ClassCastException e){
            Log.e(TAG, "onAttach: ClassCastException: " + e.getMessage() );
        }
    }

    //
    private void setupContactsList(){

        final ArrayList<Items> catalog = new ArrayList<>();
        final Items items = new Items();
        Query query;

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mRef = mFirebaseDatabase.getReference("items");
        mRef.keepSynced(true);
        query = mRef.orderByChild("name");


        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot ds : dataSnapshot.getChildren()){

                    if(mRedBook.equals("1") && mObsData.equals("0") && ds.getValue(Items.class).getRedbook().equals("1")){
                        catalog.add((Items) ds.getValue(Items.class) );
                    } else if(mObsData.equals("1") && mRedBook.equals("0") && ds.getValue(Items.class).getObsData().equals("1")){
                        catalog.add((Items) ds.getValue(Items.class) );
                    }else if(mObsData.equals("1") && mRedBook.equals("1") && ds.getValue(Items.class).getObsData().equals("1") && ds.getValue(Items.class).getRedbook().equals("1")){
                        catalog.add((Items) ds.getValue(Items.class) );
                    } else if(mRedBook.equals("0") && mObsData.equals("0")){
                        catalog.add((Items) ds.getValue(Items.class) );
                    }

                    Log.d(TAG, "showData: family: " + ds.getValue(Items.class).getFamily());
                    Log.d(TAG, "showData: name: " + ds.getValue(Items.class).getName());
                    Log.d(TAG, "showData: catalog: " + catalog.toString());
                }

                mProgressBar.setVisibility(View.GONE);

                Collections.sort(catalog, new Comparator<Items>() {
                    @Override
                    public int compare(Items o1, Items o2) {
                        return o1.getName().compareToIgnoreCase(o2.getName());
                    }
                });
                adapter = new ItemListAdapter(getActivity(), R.layout.catalog_item, catalog, "");
                catalogList.setAdapter(adapter);

                catalogList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Log.d(TAG, "katalogo seima " + catalog.get(0).getFamily());
                        //pass the contact to the interface and send it to MainActivity
                        mCatalogListener.OnCatalogSelected(catalog.get(position));
                    }
                });

                mSearchItems.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        String text = mSearchItems.getText().toString().toLowerCase(Locale.getDefault());
                        adapter.filter(text,mOrder,mFamily,mLname, mName);
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        Log.d("TAG", "catalog " + catalog.toString());
    }

    /**
     * Initiates the appbar state toggle
     */
    private void toggleToolBarState() {
        Log.d(TAG, "toggleToolBarState: toggling AppBarState.");
        if(mAppBarState == STANDARD_APPBAR){
            setAppBarState(SEARCH_APPBAR);
        }else{
            setAppBarState(STANDARD_APPBAR);
        }
    }

    /**
     * Sets the appbar state for either the search 'mode' or 'standard' mode
     * @param state
     */
    private void setAppBarState(int state) {
        Log.d(TAG, "setAppBarState: changing app bar state to: " + state);

        mAppBarState = state;

        if(mAppBarState == STANDARD_APPBAR){
            searchBar.setVisibility(View.GONE);
            viewCatalogBar.setVisibility(View.VISIBLE);

            //hide the keyboard
            View view = getView();
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            try{
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }catch (NullPointerException e){
                Log.d(TAG, "setAppBarState: NullPointerException: " + e.getMessage());
            }
        }

        else if(mAppBarState == SEARCH_APPBAR){
            viewCatalogBar.setVisibility(View.GONE);
            searchBar.setVisibility(View.VISIBLE);

            //open the keyboard
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        setAppBarState(STANDARD_APPBAR);
    }

    public boolean isConnected() throws InterruptedException, IOException {
        final String command = "ping -c 1 google.com";
        return Runtime.getRuntime().exec(command).waitFor() == 0;
    }
}
