package zenkeviciute.migle.bbd;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.maps.android.clustering.ClusterManager;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import zenkeviciute.migle.bbd.model.CatalogItems;
import zenkeviciute.migle.bbd.model.ClusterMarkers;
import zenkeviciute.migle.bbd.model.MapObservation;
import zenkeviciute.migle.bbd.ui.LoginActivity;
import zenkeviciute.migle.bbd.util.BottomNavigationViewHelper;
import zenkeviciute.migle.bbd.util.MyClusterManagerRenderer;
import zenkeviciute.migle.bbd.util.UniversalImageLoader;

public class MapActivity extends AppCompatActivity implements
        MapFragment.OnCatalogSelectedListener,
        MapFragment.OnAddCatalogListener{

    private static final String TAG = "MainActivity";

    private static final int REQUEST_CODE = 1;
    private GoogleMap mGoogleMap;
    private ClusterManager<ClusterMarkers> mClusterManager;
    private MyClusterManagerRenderer mClusterManagerRenderer;
    private ArrayList<ClusterMarkers> mClusterMarkers = new ArrayList<>();
    private FirebaseFirestore mDb;
    private ListenerRegistration mObservationListEventListener;
    private ArrayList<MapObservation> mLocation = new ArrayList<>();
    private BottomNavigationViewEx bottomNavigationViewEx;

    @Override
    public void onAddCatalog() {
        Log.d(TAG, "onAddContact: navigating to " + getString(R.string.add_catalog_fragment));

        AddMapObservationFragment fragment = new AddMapObservationFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(getString(R.string.add_catalog_fragment));
        transaction.commit();
    }

    @Override
    public void OnCatalogSelected(CatalogItems catalog) {
        Log.d(TAG, "OnContactSelected: contact selected from "
                + getString(R.string.catalog_fragment)
                + " " + catalog.getName());
//
//        CatalogItemFragment fragment = new CatalogItemFragment();
//        Bundle args = new Bundle();
//        args.putParcelable(getString(R.string.catalog), catalog);
//        fragment.setArguments(args);
//
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.fragment_container, fragment);
//        transaction.addToBackStack(getString(R.string.catalog_fragment));
//        transaction.commit();

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: started.");

        bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        bottomNavigationViewEx.setSelectedItemId(R.id.nav_map);
        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_chat:
                        Intent intent1 = new Intent(MapActivity.this, LoginActivity.class);
                        intent1.putExtra("redirect", "chat");
                        startActivity(intent1);
                        break;
                    case R.id.nav_catalog:
                        Intent intent2 = new Intent(MapActivity.this, Catalog.class);
                        startActivity(intent2);
                        break;
                    case R.id.nav_items:
                        Intent intent3 = new Intent(MapActivity.this, Item.class);
                        startActivity(intent3);
                        break;
                    case R.id.nav_map:
                        item.setChecked(true);
                        break;
                }
                return false;
            }
        });

        initImageLoader();


        init();

    }

    public void getItems(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //CollectionReference itemsColRef = db.collection("Observations");
//        DocumentReference locationsRef = db
//                .collection("Observations")
//
//        locationsRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
//            @Override
//            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
//
//                if(task.isSuccessful()){
//                    if(task.getResult().toObject(UserLocation.class) != null){
//
//                        mLocation.add(task.getResult().toObject(MapObservation.class));
//                    }
//                }
//            }
//        });

        CollectionReference usersRef = db
                .collection("Observations");

        mObservationListEventListener = usersRef
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.e(TAG, "onEvent: Listen failed.", e);
                            return;
                        }

                        if(queryDocumentSnapshots != null){

                            // Clear the list and add all the users again
                            //mLocation.clear();
                            //mLocation = new ArrayList<>();

                            for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                                MapObservation map = doc.toObject(MapObservation.class);
                                mLocation.add(map);
                                getLocation(map);
                            }

                            for(MapObservation map : mLocation){
                                Log.d(TAG, "lat " + map.getGeo_point().getLatitude());
                                Log.d(TAG, "long " + map.getGeo_point().getLatitude());
                            }

                            Log.d(TAG, "onEvent: user list size: " + mLocation.size());
                        }
                    }
                });
    }

    public void getLocation(MapObservation map){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference locRef = db.collection("Observations").document();

        locRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    if(task.getResult().toObject(MapObservation.class) != null){
                        mLocation.add(task.getResult().toObject(MapObservation.class));
                    }
                }
            }
        });

    }

    /**
     * initialize the first fragment (ViewContactsFragment)
     */
    private void init(){
        MapFragment fragment = new MapFragment();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        // reaplce whatever is in the fragment_container view with this fragment,
        // amd add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.fragment_container, fragment);

        transaction.commit();
    }

    private void initImageLoader(){
        UniversalImageLoader universalImageLoader = new UniversalImageLoader(MapActivity.this);
        ImageLoader.getInstance().init(universalImageLoader.getConfig());
    }

    @Override
    protected void onResume() {
        super.onResume();
        bottomNavigationViewEx.setSelectedItemId(R.id.nav_map);

    }

}


//package zenkeviciute.migle.bbd;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v7.app.AppCompatActivity;
//import android.view.View;
//import android.widget.TextView;
//
//import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
//import com.google.android.gms.common.GooglePlayServicesRepairableException;
//import com.google.android.gms.location.places.Place;
//import com.google.android.gms.location.places.ui.PlacePicker;
//
//import org.w3c.dom.Text;
//
//public class MapActivity extends AppCompatActivity {
//
//    private TextView getPlace;
//    int PLACE_PICKER_REQUEST = 1;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_map);
//        getPlace = (TextView) findViewById(R.id.selectLocation);
//        getPlace.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//
//                Intent intent;
//                try{
//                    intent = builder.build(MapActivity.this);
//                    startActivityForResult(intent, PLACE_PICKER_REQUEST);
//                   // Place place = PlacePicker.getPlace(this, data);
//                } catch ( GooglePlayServicesRepairableException e){
//                    e.printStackTrace();
//                } catch (GooglePlayServicesNotAvailableException e){
//                    e.printStackTrace();
//                }
//            }
//        });
//
//
//    }
//
//    protected void onActivityResult(int requestCode, int resultCode, Intent data){
//        if(requestCode==PLACE_PICKER_REQUEST){
//            if(resultCode==RESULT_OK){
//                Place place = PlacePicker.getPlace(this, data);
//                double latitude = place.getLatLng().latitude;
//                double longitude = place.getLatLng().longitude;
//                String address = String.format("Place to: " + latitude + " and " + longitude);
//                getPlace.setText(address);
//            }
//
//        }
//    }
//}
