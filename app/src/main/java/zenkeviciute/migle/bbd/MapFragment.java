package zenkeviciute.migle.bbd;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;

import zenkeviciute.migle.bbd.adapter.CatalogListAdapter;
import zenkeviciute.migle.bbd.model.CatalogItems;
import zenkeviciute.migle.bbd.model.ClusterMarkers;
import zenkeviciute.migle.bbd.model.Items;
import zenkeviciute.migle.bbd.model.MapObservation;
import zenkeviciute.migle.bbd.model.User;
import zenkeviciute.migle.bbd.ui.LoginActivity;
import zenkeviciute.migle.bbd.util.MyClusterManagerRenderer;
import zenkeviciute.migle.bbd.util.UserClient;

import static zenkeviciute.migle.bbd.util.Constants.ERROR_DIALOG_REQUEST;
import static zenkeviciute.migle.bbd.util.Constants.MAPVIEW_BUNDLE_KEY;
import static zenkeviciute.migle.bbd.util.Constants.PERMISSIONS_REQUEST_ENABLE_GPS;

public class MapFragment extends Fragment implements OnMapReadyCallback{

    private static final String TAG = "ViewContactsFragment";
    private String testImageURL = "pbs.twimg.com/profile_images/616076655547682816/6gMRtQyY.jpg";
    private CatalogListAdapter adapter;
    private Context mContext;
    private TextView mFilter;
    private String mImage;

    private DatabaseReference mRef;
    private FirebaseDatabase mFirebaseDatabase;


    private MapView mMapView;

    public interface OnCatalogSelectedListener{
        public void OnCatalogSelected(CatalogItems catalog);
    }
    OnCatalogSelectedListener mCatalogListener;

    public interface OnAddCatalogListener{
        public void onAddCatalog();
    }
    OnAddCatalogListener mOnAddCatalog;

    //variables and widgets
    private static final int STANDARD_APPBAR = 0;
    private static final int SEARCH_APPBAR = 1;
    private int mAppBarState;


    private AppBarLayout viewCatalogBar, searchBar;
    private ListView catalogList;

    private boolean mLocationPermissionGranted = false;
    private ArrayList<MapObservation> mLocation = new ArrayList<>();
    //private FusedLocationProviderClient mFusedLocationClient;
    private ListenerRegistration mObservationListEventListener;
    private GoogleMap mGoogleMap;
    private ClusterManager<ClusterMarkers> mClusterManager;
    private MyClusterManagerRenderer mClusterManagerRenderer;
    private ArrayList<ClusterMarkers> mClusterMarkers = new ArrayList<>();
    private FirebaseFirestore mDb;
    private android.support.v7.widget.Toolbar toolbar;
    private String mRedBook = "0", mObsData ="0", mPersonal ="0";
    private boolean allowRefresh = true;
    private ImageView ivExit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.map_fragment, container, false);
        //viewCatalogBar = (AppBarLayout) view.findViewById(R.id.viewCatalogToolbar);
        //catalogList = (ListView) view.findViewById(R.id.catalogList);
        Log.d(TAG, "onCreateView: started.");
        toolbar = (android.support.v7.widget.Toolbar) view.findViewById(R.id.catalogToolbar);
        ivExit = (ImageView) view.findViewById(R.id.ivExit);
        ivExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signOut();
            }
        });

        mMapView = view.findViewById(R.id.item_list_map);

        //required for setting up the toolbar
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);

        initGoogleMap(savedInstanceState);

        //setupCatalogList();

        // navigate to add contacts fragment
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.addObservation);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked fab.");
                mOnAddCatalog.onAddCatalog();
            }
        });

        mFilter = (TextView) view.findViewById(R.id.filter);
        mFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mClusterManager == null){
                    mClusterManager = new ClusterManager<ClusterMarkers>(getActivity().getApplicationContext(), mGoogleMap);
                }
                mClusterManager.clearItems();
                mClusterMarkers.clear();

                AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
                View searchView = getLayoutInflater().inflate(R.layout.filter_dialog, null);
                mBuilder.setTitle("Filtras");
                final CheckBox redBook = (CheckBox) searchView.findViewById(R.id.redBook);
                final CheckBox obsData = (CheckBox) searchView.findViewById(R.id.obsData);
                final CheckBox personal = (CheckBox) searchView.findViewById(R.id.personal);

                if(mRedBook.equals("1")){
                    redBook.setChecked(true);
                }
                if(mObsData.equals("1")){
                    obsData.setChecked(true);
                }
                if(mPersonal.equals("1")){
                    personal.setChecked(true);
                }

                mBuilder.setPositiveButton("Patvirtinti", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {

                        if(redBook.isChecked()){
                            mRedBook = "1";
                        } else {
                            mRedBook = "0";
                        }

                        if(obsData.isChecked()){
                            mObsData = "1";
                        } else {
                            mObsData = "0";
                        }

                        if(personal.isChecked()){
                            mPersonal = "1";
                        } else {
                            mPersonal = "0";
                        }

                        Log.d("mRedBook", " " + mRedBook);
                        Log.d("obsData", " " + mObsData);
                        addItems();
                    }
                });

                mBuilder.setNegativeButton("Atšaukti", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                mBuilder.setView(searchView);
                AlertDialog mDialog = mBuilder.create();
                mDialog.show();
            }
        });

        mDb = FirebaseFirestore.getInstance();

        addItems();

        return view;
    }

    private void signOut(){
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
//        finish();
    }

    public void addItems(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        CollectionReference usersRef = db
                .collection("Observations");

        mObservationListEventListener = usersRef
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.e(TAG, "onEvent: Listen failed.", e);
                            return;
                        }

                        if(queryDocumentSnapshots != null){

                            // Clear the list and add all the users again
                            //mLocation.clear();
                            //mLocation = new ArrayList<>();

                            for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                                MapObservation map = doc.toObject(MapObservation.class);
                                mLocation.add(map);
                            }


                            for(MapObservation map : mLocation){
                                Log.d("TAG", "useris " + map.getUser().getUsername());

                                markerItem(map);

                            }

                            Log.d(TAG, "onEvent: user list size: " + mLocation.size());
                        }
                    }
                });
    }

    public void addMarkerItem(MapObservation map){

        Double latitude = map.getGeo_point().getLatitude();
        Double longtitude = map.getGeo_point().getLongitude();
        String title = map.getName();
        //String snippet = map.getDate();
        String snippet = map.getDate() + "\n" +
                "Aprašymas: " + map.getDescription() + "\n" +
                "Kiekis: " + map.getAmount();
        String image = map.getImage();
        addMapMarkers(latitude, longtitude, title, snippet, image);

    }

    public void markerItem(final MapObservation map){

        try{

            final ArrayList<Items> catalog = new ArrayList<>();
            final Items items = new Items();
            Query query;


            final User user = ((UserClient)(getActivity().getApplicationContext())).getUser();

            mFirebaseDatabase = FirebaseDatabase.getInstance();
            mRef = mFirebaseDatabase.getReference("items");
            mRef.keepSynced(true);
            query = mRef.orderByChild("name").equalTo(map.getName());

            query.addListenerForSingleValueEvent(new ValueEventListener() {

                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for(DataSnapshot ds : dataSnapshot.getChildren()) {
                        mImage = ds.getValue(Items.class).getLname().replace(" ", "").toLowerCase();


                        try {


                            if (mPersonal.equals("1") && map.getUser().getUsername().equals(user.getUsername())) {
                                if (mRedBook.equals("1") && mObsData.equals("0") && ds.getValue(Items.class).getRedbook().equals("1")) {
                                    Log.d(TAG, "showData: pirmas: " + ds.getValue(Items.class).getFamily());
                                    addMarkerItem(map);
                                } else if (mObsData.equals("1") && mRedBook.equals("1") && ds.getValue(Items.class).getObsData().equals("1") && ds.getValue(Items.class).getRedbook().equals("1")) {
                                    Log.d(TAG, "showData: trecias: " + ds.getValue(Items.class).getFamily());
                                    addMarkerItem(map);
                                } else if (mObsData.equals("1") && mRedBook.equals("0") && ds.getValue(Items.class).getObsData().equals("1")) {
                                    Log.d(TAG, "showData: antras: " + ds.getValue(Items.class).getFamily());
                                    addMarkerItem(map);
                                } else if (mRedBook.equals("0") && mObsData.equals("0")) {
                                    Log.d(TAG, "showData: ketvirtas: " + ds.getValue(Items.class).getFamily());
                                    addMarkerItem(map);
                                }

                                Log.d(TAG, "showData: family: " + ds.getValue(Items.class).getFamily());
                                Log.d(TAG, "showData: name: " + ds.getValue(Items.class).getName());
                                Log.d(TAG, "showData: catalog: " + catalog.toString());
                            } else if (mPersonal.equals("0")) {

                                if (mRedBook.equals("1") && mObsData.equals("0") && ds.getValue(Items.class).getRedbook().equals("1")) {
                                    Log.d(TAG, "showData: pirmas: " + ds.getValue(Items.class).getFamily());
                                    addMarkerItem(map);
                                } else if (mObsData.equals("1") && mRedBook.equals("1") && ds.getValue(Items.class).getObsData().equals("1") && ds.getValue(Items.class).getRedbook().equals("1")) {
                                    Log.d(TAG, "showData: trecias: " + ds.getValue(Items.class).getFamily());
                                    addMarkerItem(map);
                                } else if (mObsData.equals("1") && mRedBook.equals("0") && ds.getValue(Items.class).getObsData().equals("1")) {
                                    Log.d(TAG, "showData: antras: " + ds.getValue(Items.class).getFamily());
                                    addMarkerItem(map);
                                } else if (mRedBook.equals("0") && mObsData.equals("0")) {
                                    Log.d(TAG, "showData: ketvirtas: " + ds.getValue(Items.class).getFamily());
                                    addMarkerItem(map);
                                }

                            }
                        } catch(NullPointerException er){

                        }
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


        } catch (IndexOutOfBoundsException error) {

        }
    }

    private void initGoogleMap(Bundle savedInstanceState) {

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }

        mMapView.onCreate(mapViewBundle);

        mMapView.getMapAsync( this);
    }

    private void addMapMarkers(Double latitude, Double longtitude, String title, String snippet, String image){


        if(mGoogleMap != null){

            if(mClusterManager == null){
                mClusterManager = new ClusterManager<ClusterMarkers>(getActivity().getApplicationContext(), mGoogleMap);
            }
            if(mClusterManagerRenderer == null){
                mClusterManagerRenderer = new MyClusterManagerRenderer(
                        getActivity(),
                        mGoogleMap,
                        mClusterManager
                );
                mClusterManager.setRenderer(mClusterManagerRenderer);
            }


                try{
                Log.d("TAG", "image " + image);

                    Log.d("TAG", "mImage sitas " + mImage);
                    int resID = getResources().getIdentifier(mImage, "raw", getActivity().getPackageName());
                    Log.d("TAG", "resID" + resID);
                    int avatar = resID; // set th1e default avatar
                    try{
                        avatar = Integer.parseInt(mImage);
                    }catch (NumberFormatException e){
                        Log.d(TAG, "addMapMarkers: no avatar for " + image + ", setting default.");
                    }
                    ClusterMarkers newClusterMarker = new ClusterMarkers(
                            new LatLng(latitude, longtitude),
                            title,
                            snippet,
                            avatar
                    );
                    mClusterManager.addItem(newClusterMarker);
                    mClusterMarkers.add(newClusterMarker);

                    mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                        @Override
                        public View getInfoWindow(Marker arg0) {
                            return null;
                        }

                        @Override
                        public View getInfoContents(Marker marker) {

                            Context context = getActivity(); //or getActivity(), YourActivity.this, etc.

                            LinearLayout info = new LinearLayout(context);
                            info.setOrientation(LinearLayout.VERTICAL);

                            TextView title = new TextView(context);
                            title.setTextColor(Color.BLACK);
                            title.setGravity(Gravity.CENTER);
                            title.setTypeface(null, Typeface.BOLD);
                            title.setText(marker.getTitle());

                            TextView snippet = new TextView(context);
                            snippet.setTextColor(Color.GRAY);
                            snippet.setText(marker.getSnippet());

                            info.addView(title);
                            info.addView(snippet);

                            return info;
                        }
                    });


                }catch (NullPointerException e){
                    Log.e(TAG, "addMapMarkers: NullPointerException: " + e.getMessage() );
                }
            mClusterManager.cluster();
        }
    }

    private boolean checkMapServices(){
        if(isServicesOK()){
            if(isMapsEnabled()){
                return true;
            }
        }
        return false;
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Įjunkite GPS norėdami patikslinti savo vietą.")
                .setNegativeButton("Atšaukti", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Gerai", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        Intent enableGpsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(enableGpsIntent, PERMISSIONS_REQUEST_ENABLE_GPS);
                    }
                });


        final AlertDialog alert = builder.create();
        alert.show();
    }

    public boolean isMapsEnabled(){
        final LocationManager manager = (LocationManager) getActivity().getSystemService( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
            return false;
        }
        return true;
    }

    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
//        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
//                Manifest.permission.ACCESS_FINE_LOCATION)
//                == PackageManager.PERMISSION_GRANTED) {
//            mLocationPermissionGranted = true;
//            //getLastKnownLocation();
//            //getChatrooms();
//        } else {
////            ActivityCompat.requestPermissions(getActivity(),
////                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
////                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
//        }
    }

    public boolean isServicesOK(){
        Log.d(TAG, "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getActivity());

        if(available == ConnectionResult.SUCCESS){
            //everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        }
        else if(GoogleApiAvailability.getInstance().isUserResolvableError(available)){
            //an error occured but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), available, ERROR_DIALOG_REQUEST);
            dialog.show();
        }else{
            Toast.makeText(getActivity(), "Klaida", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
//        mLocationPermissionGranted = false;
//        switch (requestCode) {
//            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    mLocationPermissionGranted = true;
//                }
//            }
//        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: called.");
        addItems();
//        switch (requestCode) {
//            case PERMISSIONS_REQUEST_ENABLE_GPS: {
//                if(mLocationPermissionGranted){
//                    //getLastKnownLocation();
//                    //getChatrooms();
//                }
//                else{
//                    getLocationPermission();
//                }
//            }
//        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            mCatalogListener = (OnCatalogSelectedListener) getActivity();
            mOnAddCatalog = (OnAddCatalogListener) getActivity();
        }catch (ClassCastException e){
            Log.e(TAG, "onAttach: ClassCastException: " + e.getMessage() );
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        addItems();
        if(checkMapServices()){
            if(mLocationPermissionGranted){
               // getLastKnownLocation();
                //getChatrooms();

            }
            else{
                getLocationPermission();
            }
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        map.setMyLocationEnabled(true);
        mGoogleMap = map;

        addItems();

    }

    @Override
    public void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}
