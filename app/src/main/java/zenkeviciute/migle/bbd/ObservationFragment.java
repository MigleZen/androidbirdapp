package zenkeviciute.migle.bbd;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import zenkeviciute.migle.bbd.adapter.ObservationListAdapter;
import zenkeviciute.migle.bbd.database.ObservationDatabase;
import zenkeviciute.migle.bbd.model.CatalogItems;
import zenkeviciute.migle.bbd.model.ObservationItems;

public class ObservationFragment extends Fragment {

    private static final String TAG = "ViewContactsFragment";
    private String testImageURL = "pbs.twimg.com/profile_images/616076655547682816/6gMRtQyY.jpg";
    private ObservationListAdapter adapter;


    public interface OnCatalogSelectedListener{
        public void OnCatalogSelected(ObservationItems catalog);
    }
    OnCatalogSelectedListener mCatalogListener;

    public interface OnAddCatalogListener{
        public void onAddCatalog();
    }
    OnAddCatalogListener mOnAddCatalog;

    //variables and widgets
    private static final int STANDARD_APPBAR = 0;
    private static final int SEARCH_APPBAR = 1;
    private int mAppBarState;


    private AppBarLayout viewCatalogBar, searchBar;
    private ListView catalogList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.catalog_fragment, container, false);
        viewCatalogBar = (AppBarLayout) view.findViewById(R.id.viewCatalogToolbar);
        catalogList = (ListView) view.findViewById(R.id.catalogList);
        Log.d(TAG, "onCreateView: started.");

        setupCatalogList();

        // navigate to add contacts fragment
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.addCatalog);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked fab.");
                AddObservationFragment fragment = new AddObservationFragment();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, fragment);
                transaction.addToBackStack(getString(R.string.add_catalog_fragment));
                transaction.commit();
            }
        });
        ImageView ivBackArrow = (ImageView) view.findViewById(R.id.ivBackArrow);
        ivBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked back arrow.");
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            mCatalogListener = (OnCatalogSelectedListener) getActivity();
            mOnAddCatalog = (OnAddCatalogListener) getActivity();
        }catch (ClassCastException e){
            Log.e(TAG, "onAttach: ClassCastException: " + e.getMessage() );
        }
    }

    //
    private void setupCatalogList() {
        final ArrayList<ObservationItems> catalog = new ArrayList<>();

        ObservationDatabase databaseHelper = new ObservationDatabase(getActivity());
        Cursor cursor = databaseHelper.getAllCatalogs();

        //iterate through all the rows contained in the database
        if (!cursor.moveToNext()) {
            Toast.makeText(getActivity(), "Nėra įrašų", Toast.LENGTH_SHORT).show();
        }
        cursor.moveToPosition(-1);
        while(cursor.moveToNext()) {
            catalog.add(new ObservationItems(
                    cursor.getString(1),//name
                    cursor.getString(2),//location
                    cursor.getString(3),//description
                    cursor.getString(4),//picture
                    cursor.getString(5),//name
                    cursor.getString(6),//name
                    cursor.getString(7)//name
            ));
        }
        cursor.close();

        Log.d(TAG, "setupContactsList: image url: " + catalog.toString());

        adapter = new ObservationListAdapter(getActivity(), R.layout.catalog_item, catalog, "");
        catalogList.setAdapter(adapter);

        catalogList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onClick: navigating to " + getString(R.string.catalog_fragment));

                //pass the contact to the interface and send it to MainActivity
                ObservationItemFragment fragment = new ObservationItemFragment();
                Bundle args = new Bundle();
                args.putParcelable(getString(R.string.catalog), catalog.get(position));
                fragment.setArguments(args);

                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, fragment);
                transaction.addToBackStack(getString(R.string.edit_catalog_fragment));
                transaction.commit();
            }
        });
    }
}
