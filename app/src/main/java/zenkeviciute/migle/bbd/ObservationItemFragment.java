package zenkeviciute.migle.bbd;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import de.hdodenhof.circleimageview.CircleImageView;
import zenkeviciute.migle.bbd.database.ObservationDatabase;
import zenkeviciute.migle.bbd.model.ObservationItems;
import zenkeviciute.migle.bbd.util.UniversalImageLoader;

public class ObservationItemFragment extends Fragment {

    private static final String TAG = "AddCatalogFragment";

    public ObservationItemFragment(){
        super();
        setArguments(new Bundle());
    }

    public interface OnEditCatalogListener{
        public void onEditCatalogSelected(ObservationItems catalog);
    }

    OnEditCatalogListener mOnEditCatalogListener;

    //private Contact mContact;

    public TextView mCatalogName, mName, mDescription, mAmount, mDate, mLocation;
    private CircleImageView mCatalogImage;
    private Toolbar toolbar;
    private String mSelectedImagePath;
    private ObservationItems mCatalog;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.observation_item_fragment, container, false);
        mCatalogName = (TextView) view.findViewById(R.id.etCatalogName);
        mLocation = (TextView) view.findViewById(R.id.etLocation);
        mName = (TextView) view.findViewById(R.id.etName);
        mDescription = (TextView) view.findViewById(R.id.etDescription);
        mCatalogImage = (CircleImageView) view.findViewById(R.id.catalogImage);
        mAmount = (TextView) view.findViewById(R.id.etAmount);
        mDate = (TextView) view.findViewById(R.id.etDate);
        toolbar = (Toolbar) view.findViewById(R.id.editCatalogToolbar);
        mCatalog = getCatalogFromBundle();

        //required for setting up the toolbar
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);

        init();

        //navigation for the backarrow
        ImageView ivBackArrow = (ImageView) view.findViewById(R.id.ivBackArrow);
        ivBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked back arrow.");
                //remove previous fragment from the backstack (therefore navigating back)
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        // navigate to the edit contact fragment to edit the contact selected
        ImageView ivEdit = (ImageView) view.findViewById(R.id.ivEdit);
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked the edit icon.");
                EditObservationFragment fragment = new EditObservationFragment();
                Bundle args = new Bundle();
                args.putParcelable(getString(R.string.catalog), mCatalog);
                fragment.setArguments(args);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, fragment);
                transaction.addToBackStack(getString(R.string.edit_catalog_fragment));
                transaction.commit();
            }
        });

        return view;
    }

    private void init(){
        mCatalogName.setText(mCatalog.getCatalog());
        mLocation.setText(mCatalog.getLocation());
        mName.setText(mCatalog.getName());
        mDescription.setText(mCatalog.getDescription());
        mAmount.setText(mCatalog.getAmount());
        mDate.setText(mCatalog.getDate());

        UniversalImageLoader.setImage(mCatalog.getImage(), mCatalogImage, null, "");
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            mOnEditCatalogListener = (OnEditCatalogListener) getActivity();
        }catch (ClassCastException e){
            Log.e(TAG, "onAttach: ClassCastException: " + e.getMessage() );
        }
    }


    private ObservationItems getCatalogFromBundle(){
        Log.d(TAG, "getContactFromBundle: arguments AddObservationFragment: " + getArguments());

        Bundle bundle = this.getArguments();
        if(bundle != null){
            return bundle.getParcelable(getString(R.string.catalog));
        }else{
            return null;
        }
    }

    private boolean checkStringIfNull(String string){
        if(string.equals("")){
            return false;
        }else{
            return true;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.catalog_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menuitem_delete:
                ObservationDatabase databaseHelper = new ObservationDatabase(getActivity());
                Cursor cursor = databaseHelper.getCatalogID(mCatalog);

                int contactID = -1;
                while(cursor.moveToNext()){
                    contactID = cursor.getInt(0);
                }
                if(contactID > -1){
                    if(databaseHelper.deleteCatalog(contactID) > 0){
                        Toast.makeText(getActivity(), "Contact Deleted", Toast.LENGTH_SHORT).show();

                        //clear the arguments ont he current bundle since the contact is deleted
                        this.getArguments().clear();

                        //remove previous fragemnt from the backstack (therefore navigating back)
                        getActivity().getSupportFragmentManager().popBackStack();
                    }
                    else{
                        Toast.makeText(getActivity(), "Database Error", Toast.LENGTH_SHORT).show();
                    }
                }
        }
        return super.onOptionsItemSelected(item);
    }
}