package zenkeviciute.migle.bbd;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;

import zenkeviciute.migle.bbd.model.ClusterMarkers;
import zenkeviciute.migle.bbd.model.Items;
import zenkeviciute.migle.bbd.model.MapObservation;
import zenkeviciute.migle.bbd.model.User;
import zenkeviciute.migle.bbd.util.MyClusterManagerRenderer;
import zenkeviciute.migle.bbd.util.UserClient;

import static zenkeviciute.migle.bbd.util.Constants.MAPVIEW_BUNDLE_KEY;

public class UserMap extends AppCompatActivity implements OnMapReadyCallback{

    private User mUser;
    private MapView mMapView;

    //variables and widgets
    private static final int STANDARD_APPBAR = 0;
    private static final int SEARCH_APPBAR = 1;
    private int mAppBarState;

    private AppBarLayout viewCatalogBar, searchBar;
    private ListView catalogList;

    private boolean mLocationPermissionGranted = false;
    private ArrayList<MapObservation> mLocation = new ArrayList<>();
    private ListenerRegistration mObservationListEventListener;
    private GoogleMap mGoogleMap;
    private ClusterManager<ClusterMarkers> mClusterManager;
    private MyClusterManagerRenderer mClusterManagerRenderer;
    private ArrayList<ClusterMarkers> mClusterMarkers = new ArrayList<>();
    private FirebaseFirestore mDb;
    private android.support.v7.widget.Toolbar toolbar;

    private String mUsername;
    private TextView mtoolbarName;
    private String mImage;

    private DatabaseReference mRef;
    private FirebaseDatabase mFirebaseDatabase;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_map);

        Log.d("TAG", "msg username cia");
        if (getIntent().hasExtra("user")) {
            mUser = getIntent().getParcelableExtra("user");
            mUsername = mUser.getUsername();
            Log.d("TAG", "msg username" + mUser.getUsername());
        }

        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.catalogToolbar);
        mtoolbarName = findViewById(R.id.textCatalogToolbar);
        mtoolbarName.setText(mUsername + " stebėjimai");

        mMapView = findViewById(R.id.item_list_map);
        initGoogleMap(savedInstanceState);
        addItems();

    }


    public void markerItem(final MapObservation map){

        try{

            final ArrayList<Items> catalog = new ArrayList<>();
            final Items items = new Items();
            Query query;


            final User user = ((UserClient)(this.getApplicationContext())).getUser();

            mFirebaseDatabase = FirebaseDatabase.getInstance();
            mRef = mFirebaseDatabase.getReference("items");
            mRef.keepSynced(true);
            query = mRef.orderByChild("name").equalTo(map.getName());

            query.addListenerForSingleValueEvent(new ValueEventListener() {

                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for(DataSnapshot ds : dataSnapshot.getChildren()) {
                        mImage = ds.getValue(Items.class).getLname().replace(" ", "").toLowerCase();

                        if (map.getUser().getUsername().equals(mUsername)) {
                            Double latitude = map.getGeo_point().getLatitude();
                            Double longtitude = map.getGeo_point().getLongitude();
                            String title = map.getName();
                            //String snippet = map.getDate();
                            String snippet = map.getDate() + "\n" +
                                    "Aprašymas: " + map.getDescription() + "\n" +
                                    "Kiekis: " + map.getAmount();
                            String image = map.getImage();
                            addMapMarkers(latitude, longtitude, title, snippet, image);
                        }
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


        } catch (IndexOutOfBoundsException error) {

        }
    }

    public void addItems(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        CollectionReference usersRef = db
                .collection("Observations");

        mObservationListEventListener = usersRef
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                           // Log.e(TAG, "onEvent: Listen failed.", e);
                            return;
                        }

                        if(queryDocumentSnapshots != null){

                            for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                                MapObservation map = doc.toObject(MapObservation.class);
                                mLocation.add(map);
                            }


                            for(MapObservation map : mLocation){
                                Log.d("TAG", "useris " + map.getUser().getUsername());
                                markerItem(map);
                            }
                        }
                    }
                });
    }
    private void initGoogleMap(Bundle savedInstanceState) {
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }

        mMapView.onCreate(mapViewBundle);

        mMapView.getMapAsync( this);
    }

    private void addMapMarkers(Double latitude, Double longtitude, String title, String snippet, String image){


        if(mGoogleMap != null){

            if(mClusterManager == null){
                mClusterManager = new ClusterManager<ClusterMarkers>(this.getApplicationContext(), mGoogleMap);
            }
            if(mClusterManagerRenderer == null){
                mClusterManagerRenderer = new MyClusterManagerRenderer(
                        this,
                        mGoogleMap,
                        mClusterManager
                );
                mClusterManager.setRenderer(mClusterManagerRenderer);
            }


            try{
                Log.d("TAG", "image " + image);
                Log.d("TAG", "image " + image);
                int resID = getResources().getIdentifier(mImage , "raw", this.getPackageName());
                Log.d("TAG", "resID" + resID);
                int avatar = resID;
                try{
                    avatar = Integer.parseInt(mImage);
                }catch (NumberFormatException e){

                }
                ClusterMarkers newClusterMarker = new ClusterMarkers(
                        new LatLng(latitude, longtitude),
                        title,
                        snippet,
                        avatar
                );
                mClusterManager.addItem(newClusterMarker);
                mClusterMarkers.add(newClusterMarker);

                mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                    @Override
                    public View getInfoWindow(Marker arg0) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(Marker marker) {

                        Context context = UserMap.this;

                        LinearLayout info = new LinearLayout(context);
                        info.setOrientation(LinearLayout.VERTICAL);

                        TextView title = new TextView(context);
                        title.setTextColor(Color.BLACK);
                        title.setGravity(Gravity.CENTER);
                        title.setTypeface(null, Typeface.BOLD);
                        title.setText(marker.getTitle());

                        TextView snippet = new TextView(context);
                        snippet.setTextColor(Color.GRAY);
                        snippet.setText(marker.getSnippet());

                        info.addView(title);
                        info.addView(snippet);

                        return info;
                    }
                });


            }catch (NullPointerException e){
               // Log.e(TAG, "addMapMarkers: NullPointerException: " + e.getMessage() );
            }
            mClusterManager.cluster();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        map.setMyLocationEnabled(true);
        mGoogleMap = map;
    }

    @Override
    public void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}

