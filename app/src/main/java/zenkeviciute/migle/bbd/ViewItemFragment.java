package zenkeviciute.migle.bbd;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import zenkeviciute.migle.bbd.adapter.SectionsPageAdapter;
import zenkeviciute.migle.bbd.model.Items;

public class ViewItemFragment extends Fragment {
    private static final String TAG = "ContactFragment";

    private SectionsPageAdapter mSectionsPageAdapter;

    private ViewPager mViewPager;

    //This will evade the nullpointer exception when adding to a new bundle from MainActivity
    public ViewItemFragment(){
        super();
        setArguments(new Bundle());
    }

    private Items mCatalog;
    private TextView mName;
    private CircleImageView mImage;
    private ListView mListView;
    private Toolbar toolbar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.description, container, false);
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mCatalog = getCatalogFromBundle();
        toolbar.setTitle(mCatalog.getName());
        getActivity().getIntent().putExtra("name", mCatalog.getName());
        mSectionsPageAdapter = new SectionsPageAdapter(getActivity().getSupportFragmentManager());
        mViewPager = (ViewPager) view.findViewById(R.id.container);
        setupViewPager(mViewPager);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new DescriptionMainFragment(), "Apie rūšį");
        adapter.addFragment(new DescriptionFragment(), "Aprašymas");
        adapter.addFragment(new DescriptionHabitatFragment(), "Buveinė ir paplitimas");
        viewPager.setAdapter(adapter);
    }

    private Items getCatalogFromBundle(){
        Bundle bundle = this.getArguments();
        if(bundle != null){
            return bundle.getParcelable(getString(R.string.catalog));
        }else{
            return null;
        }
    }
}
