package zenkeviciute.migle.bbd.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import zenkeviciute.migle.bbd.R;
import zenkeviciute.migle.bbd.model.CatalogItems;

public class CatalogListAdapter extends ArrayAdapter<CatalogItems> {
    private LayoutInflater mInflater;
    private List<CatalogItems> mCatalogs = null;
    private ArrayList<CatalogItems> arrayList; //used for the search bar
    private int layoutResource;
    private Context mContext;
    private String mAppend;

    public CatalogListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<CatalogItems> catalogs, String append) {
        super(context, resource, catalogs);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutResource = resource;
        this.mContext = context;
        mAppend = append;
        this.mCatalogs = catalogs;
        arrayList = new ArrayList<>();
        this.arrayList.addAll(mCatalogs);
    }

    private static class ViewHolder{
        TextView name;
        TextView location;
        CircleImageView catalogImage;
        ProgressBar mProgressBar;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final ViewHolder holder;

        if(convertView == null){
            convertView = mInflater.inflate(layoutResource, parent, false);
            holder = new ViewHolder();

            holder.name = (TextView) convertView.findViewById(R.id.catalogName);
            holder.catalogImage = (CircleImageView) convertView.findViewById(R.id.catalogImage);
            holder.location = (TextView) convertView.findViewById(R.id.catalogSubtitle);
            holder.mProgressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }

        String name_ = getItem(position).getName();
        String location_ = getItem(position).getLocation();
        String imagePath = getItem(position).getLocationImage();
        holder.name.setText(name_);
        holder.location.setText(location_);

        ImageLoader imageLoader = ImageLoader.getInstance();

        Log.d("CatalogListAdapter", "imageLoader: " + imagePath);

        imageLoader.displayImage(mAppend + imagePath, holder.catalogImage, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                holder.mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                holder.mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                holder.mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                holder.mProgressBar.setVisibility(View.GONE);
            }
        });
        return convertView;
    }
}
