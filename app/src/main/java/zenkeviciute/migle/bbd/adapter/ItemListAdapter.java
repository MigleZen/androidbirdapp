package zenkeviciute.migle.bbd.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import zenkeviciute.migle.bbd.R;
import zenkeviciute.migle.bbd.model.CatalogItems;
import zenkeviciute.migle.bbd.model.Items;

public class ItemListAdapter extends ArrayAdapter<Items> {
    private LayoutInflater mInflater;
    private List<Items> mCatalogs = null;
    private ArrayList<Items> arrayList; //used for the search bar
    private int layoutResource;
    private Context mContext;
    private String mAppend;
    private boolean filterName;
    private Context context;

    public ItemListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Items> catalogs, String append) {
        super(context, resource, catalogs);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutResource = resource;
        this.mContext = context;
        mAppend = append;
        this.mCatalogs = catalogs;
        arrayList = new ArrayList<>();
        this.arrayList.addAll(mCatalogs);
    }

    private static class ViewHolder{
        TextView name, lname;
        CircleImageView catalogImage;
        ProgressBar mProgressBar;
    }

    @SuppressLint("ResourceType")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final ViewHolder holder;

        if(convertView == null){
            convertView = mInflater.inflate(layoutResource, parent, false);
            holder = new ViewHolder();

            holder.name = (TextView) convertView.findViewById(R.id.catalogName);
            holder.lname = (TextView) convertView.findViewById(R.id.catalogSubtitle);
            holder.catalogImage = (CircleImageView) convertView.findViewById(R.id.catalogImage);
            holder.mProgressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }

         String name_ = getItem(position).getName();
        String lname_ = getItem(position).getLname();
        String image = getItem(position).getImage();
        Log.d("tag", "image url:" + image);
        String imagePath = image;

        holder.name.setText(name_);
        holder.lname.setText(lname_);
        holder.mProgressBar.setVisibility(View.GONE);

        ImageLoader imageLoader = ImageLoader.getInstance();

        imageLoader.displayImage( imagePath, holder.catalogImage, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                holder.mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                holder.mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                holder.mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                holder.mProgressBar.setVisibility(View.GONE);
            }
        });
        //--------------------------------------------------------------------------------------

        return convertView;
    }

    //Filter class
    public void filter(String characterText, String order, String family, String lname, String name){
        characterText = characterText.toLowerCase(Locale.getDefault());
        mCatalogs.clear();
        if( characterText.length() == 0){
            mCatalogs.addAll(arrayList);
        }
        else{
            mCatalogs.clear();
            for(Items catalog: arrayList){
//                if(catalog.getName().toLowerCase(Locale.getDefault()).contains(characterText) ||
//                        catalog.getLname().toLowerCase(Locale.getDefault()).contains(characterText)){
//                    mCatalogs.add(catalog);
//                }
                if(name.equals("1")){
                    if(catalog.getName().toLowerCase(Locale.getDefault()).contains(characterText)){
                        mCatalogs.add(catalog);
                    }
                } else if(order.equals("1")) {
                    if (catalog.getOrder().toLowerCase(Locale.getDefault()).contains(characterText)) {
                        mCatalogs.add(catalog);
                    }
                } else if(lname.equals("1")) {
                    if (catalog.getLname().toLowerCase(Locale.getDefault()).contains(characterText)) {
                        mCatalogs.add(catalog);
                    }
                } else if(family.equals("1")) {
                    if (catalog.getFamily().toLowerCase(Locale.getDefault()).contains(characterText)) {
                        mCatalogs.add(catalog);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }
}
