package zenkeviciute.migle.bbd.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import zenkeviciute.migle.bbd.Observation;
import zenkeviciute.migle.bbd.R;
import zenkeviciute.migle.bbd.model.CatalogItems;
import zenkeviciute.migle.bbd.model.ObservationItems;

public class ObservationListAdapter extends ArrayAdapter<ObservationItems> {
    private LayoutInflater mInflater;
    private List<ObservationItems> mCatalogs = null;
    private ArrayList<ObservationItems> arrayList; //used for the search bar
    private int layoutResource;
    private Context mContext;
    private String mAppend;

    public ObservationListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<ObservationItems> catalogs, String append) {
        super(context, resource, catalogs);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutResource = resource;
        this.mContext = context;
        mAppend = append;
        this.mCatalogs = catalogs;
        arrayList = new ArrayList<>();
        this.arrayList.addAll(mCatalogs);
    }

    private static class ViewHolder{
        TextView name;
        TextView date;
        CircleImageView catalogImage;
        ProgressBar mProgressBar;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        /*
         ************ ViewHolder Build Pattern Start ************
         */
        final ViewHolder holder;

        if(convertView == null){
            convertView = mInflater.inflate(layoutResource, parent, false);
            holder = new ViewHolder();

            //---------------------------Stuff to change--------------------------------------------
            holder.name = (TextView) convertView.findViewById(R.id.catalogName);
            holder.date = (TextView) convertView.findViewById(R.id.catalogSubtitle);
            holder.catalogImage = (CircleImageView) convertView.findViewById(R.id.catalogImage);
            //--------------------------------------------------------------------------------------

            holder.mProgressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }

        //---------------------------Stuff to change--------------------------------------------
        String name_ = getItem(position).getName();
        String date_ = getItem(position).getDate();
        String imagePath = getItem(position).getImage();
        holder.name.setText(name_);
        holder.date.setText(date_);

        ImageLoader imageLoader = ImageLoader.getInstance();

        imageLoader.displayImage(mAppend + imagePath, holder.catalogImage, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                holder.mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                holder.mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                holder.mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                holder.mProgressBar.setVisibility(View.GONE);
            }
        });
        //--------------------------------------------------------------------------------------

        return convertView;
    }
}
