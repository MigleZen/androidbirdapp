package zenkeviciute.migle.bbd.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import zenkeviciute.migle.bbd.R;
import zenkeviciute.migle.bbd.UserMap;
import zenkeviciute.migle.bbd.model.Chatroom;
import zenkeviciute.migle.bbd.model.MapObservation;
import zenkeviciute.migle.bbd.model.ObservationItems;
import zenkeviciute.migle.bbd.model.User;
import zenkeviciute.migle.bbd.util.UserClient;

public class UserRecyclerAdapter extends RecyclerView.Adapter<UserRecyclerAdapter.ViewHolder>
{

    private ArrayList<User> mUsers = new ArrayList<>();
    private UserRecyclerClickListener mUserRecyclerClickListener;
    private FirebaseFirestore mDb;
    private ArrayList<MapObservation> mScores = new ArrayList<>();
    private Integer mCount;


    public UserRecyclerAdapter(ArrayList<User> users, UserRecyclerClickListener userRecyclerClickListener) {
        this.mUsers = users;
        mUserRecyclerClickListener = userRecyclerClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_user_list_item, parent, false);
        final ViewHolder holder = new ViewHolder(view, mUserRecyclerClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        ((ViewHolder)holder).username.setText(mUsers.get(position).getUsername());
        ((ViewHolder)holder).email.setText(mUsers.get(position).getEmail());
        Log.d("tag", "count " + mCount);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference usersRef = db
                .collection("Observations");
        usersRef
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        if (e != null) {

                            return;
                        }

                        if(queryDocumentSnapshots != null){

                            Integer count = 0;
                            for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                                MapObservation map = doc.toObject(MapObservation.class);
                                if(map.getUser().getUsername().equals(mUsers.get(position).getUsername())){
                                    Log.d("TAG", "useris count " + map.getUser().getUsername() + "  " + count);
                                    count++;
                                }
                                //mScores.add(map);
                            }

                            Log.d("TAG", " count " + count);
                            ((ViewHolder)holder).score.setText(count.toString());
                        }
                    }

                });
    }


    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener
    {
        TextView username, email, score, position;
        CardView user;
        UserRecyclerClickListener clickListener;

        public ViewHolder(View itemView, UserRecyclerClickListener clickListener) {
            super(itemView);
            username = itemView.findViewById(R.id.username);
            email = itemView.findViewById(R.id.email);
            score = itemView.findViewById(R.id.score);
            position = itemView.findViewById(R.id.position);
            this.clickListener = clickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onUserSelected(getAdapterPosition());
        }
    }

    public interface UserRecyclerClickListener {
        public void onUserSelected(int position);
    }
}
















