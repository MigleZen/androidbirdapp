package zenkeviciute.migle.bbd.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import zenkeviciute.migle.bbd.Catalog;
import zenkeviciute.migle.bbd.model.CatalogItems;

public class CreateDatabase extends SQLiteOpenHelper {
    private static final String TAG = "CreateDatabase";

    private static final String DATABASE_NAME = "catalogue.db";
    private static final String TABLE_NAME = "catalogue";
    private static final String COL0 = "ID";
    private static final String COL1 = "name";
    private static final String COL2 = "description";
    private static final String COL3 = "location";
    private static final String COL4 = "location_image";
    private static final String COL5 = "date";


    public CreateDatabase(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " +
                TABLE_NAME + " ( " +
                COL0 + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL1 + " TEXT, " +
                COL2 + " TEXT, " +
                COL3 + " TEXT, " +
                COL4 + " TEXT, " +
                COL5 + " TEXT)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    /**
     * Insert a new contact into the database
     * @param catalog
     * @return
     */
    public boolean addCatalog(CatalogItems catalog) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, catalog.getName());
        contentValues.put(COL2, catalog.getDescription());
        contentValues.put(COL3, catalog.getLocation());
        contentValues.put(COL4, catalog.getLocationImage());
        contentValues.put(COL5, catalog.getTimestamp());

        long result = db.insert(TABLE_NAME, null, contentValues);

        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Retrieve all contacts from database
     * @return
     */
    public Cursor getAllContacts(){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }

    /**
     * Update a catalog where id = @param 'id'
     * Replace the current catalog with @param 'catalog'
     * @param catalog
     * @param id
     * @return
     */
    public boolean updateCatalog(CatalogItems catalog, int id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, catalog.getName());
        contentValues.put(COL2, catalog.getDescription());
        contentValues.put(COL3, catalog.getLocation());
        contentValues.put(COL4, catalog.getLocationImage());
        contentValues.put(COL5, catalog.getTimestamp());

        int update = db.update(TABLE_NAME, contentValues, COL0 + " = ? ", new String[] {String.valueOf(id)} );

        if(update != 1) {
            return false;
        }
        else{
            return true;
        }
    }

    /**
     * Retrieve the catalog unique id from the database using @param
     * @param catalog
     * @return
     */
    public Cursor getCatalogID(CatalogItems catalog){
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "SELECT * FROM " + TABLE_NAME  +
                " WHERE " + COL1 + " = '" + catalog.getName() + "'";
        return db.rawQuery(sql, null);
    }

    public Integer deleteCatalog(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "ID = ?", new String[] {String.valueOf(id)});
    }

}
