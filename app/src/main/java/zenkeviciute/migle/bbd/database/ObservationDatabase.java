package zenkeviciute.migle.bbd.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import zenkeviciute.migle.bbd.Catalog;
import zenkeviciute.migle.bbd.model.CatalogItems;
import zenkeviciute.migle.bbd.model.ObservationItems;

public class ObservationDatabase extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "observation.db";
    private static final String TABLE_NAME = "observation";
    private static final String COL0 = "ID";
    private static final String COL1 = "catalog";
    private static final String COL2 = "name";
    private static final String COL3 = "location";
    private static final String COL4 = "amount";
    private static final String COL5 = "description";
    private static final String COL6 = "image";
    private static final String COL7 = "date";


    public ObservationDatabase(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " +
                TABLE_NAME + " ( " +
                COL0 + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL1 + " TEXT, " +
                COL2 + " TEXT, " +
                COL3 + " TEXT, " +
                COL4 + " TEXT, " +
                COL5 + " TEXT, " +
                COL6 + " TEXT, " +
                COL7 + " TEXT)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    /**
     * Insert a new contact into the database
     * @param catalog
     * @return
     */
    public boolean addCatalog(ObservationItems catalog) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, catalog.getCatalog());
        contentValues.put(COL2, catalog.getName());
        contentValues.put(COL3, catalog.getLocation());
        contentValues.put(COL4, catalog.getAmount());
        contentValues.put(COL5, catalog.getDescription());
        contentValues.put(COL6, catalog.getImage());
        contentValues.put(COL7, catalog.getDate());

        long result = db.insert(TABLE_NAME, null, contentValues);

        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Retrieve all contacts from database
     * @return
     */
    public Cursor getAllCatalogs(){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }

    public Cursor getItemsByCatalog(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "SELECT * FROM " + TABLE_NAME  +
                " WHERE catalog = '" + name + "'";
        return db.rawQuery(sql, null);
    }

    /**
     * Update a catalog where id = @param 'id'
     * Replace the current catalog with @param 'catalog'
     * @param catalog
     * @param id
     * @return
     */
    public boolean updateCatalog(ObservationItems catalog, int id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, catalog.getCatalog());
        contentValues.put(COL2, catalog.getName());
        contentValues.put(COL3, catalog.getLocation());
        contentValues.put(COL4, catalog.getAmount());
        contentValues.put(COL5, catalog.getDescription());
        contentValues.put(COL6, catalog.getImage());
        contentValues.put(COL7, catalog.getDate());

        int update = db.update(TABLE_NAME, contentValues, COL0 + " = ? ", new String[] {String.valueOf(id)} );

        if(update != 1) {
            return false;
        }
        else{
            return true;
        }
    }

    /**
     * Retrieve the catalog unique id from the database using @param
     * @param catalog
     * @return
     */
    public Cursor getCatalogID(ObservationItems catalog){
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "SELECT * FROM " + TABLE_NAME  +
                " WHERE " + COL2 + " = '" + catalog.getName() + "' AND " + COL7 + " = '" + catalog.getDate() + "'";
        return db.rawQuery(sql, null);
    }

    public Integer deleteCatalog(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "ID = ?", new String[] {String.valueOf(id)});
    }

}
