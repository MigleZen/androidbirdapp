package zenkeviciute.migle.bbd.model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;


public class CatalogItems implements Parcelable {

    private String name;
    private String description;
    private String location;
    private String locationImage;
    private String timestamp;

    public CatalogItems(String name, String description, String location, String locationImage, String timestamp) {
        this.name = name;
        this.description = description;
        this.location = location;
        this.locationImage = locationImage;
        this.timestamp = timestamp;
    }

    protected CatalogItems(Parcel in) {
        name = in.readString();
        description = in.readString();
        location = in.readString();
        locationImage = in.readString();
        timestamp = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(location);
        dest.writeString(locationImage);
        dest.writeString(timestamp);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CatalogItems> CREATOR = new Creator<CatalogItems>() {
        @Override
        public CatalogItems createFromParcel(Parcel in) {
            return new CatalogItems(in);
        }

        @Override
        public CatalogItems[] newArray(int size) {
            return new CatalogItems[size];
        }
    };

    @Override
    public String toString() {
        return "CatalogItems{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", location='" + location + '\'' +
                ", locationImage='" + locationImage + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationImage() {
        return locationImage;
    }

    public void setLocationImage(String locationImage) {
        this.locationImage = locationImage;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


}
