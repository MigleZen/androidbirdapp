package zenkeviciute.migle.bbd.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.Blob;

public class Items implements Parcelable {

    public String name;
    public String lname;
    public String order;
    public String family;
    public String category;
    public String status;
    public String redbook;
    public String lofk;
    public String obsData;
    public String habitat;
    public String population;
    public String description;
    public String image;

    public  Items(){

    }

    public Items(String name, String lname, String order, String family, String category, String status, String redbook, String lofk, String obsData, String habitat, String population, String description, String image) {
        this.name = name;
        this.lname = lname;
        this.order = order;
        this.family = family;
        this.category = category;
        this.status = status;
        this.redbook = redbook;
        this.lofk = lofk;
        this.obsData = obsData;
        this.habitat = habitat;
        this.population = population;
        this.description = description;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRedbook() {
        return redbook;
    }

    public void setRedbook(String redbook) {
        this.redbook = redbook;
    }

    public String getLofk() {
        return lofk;
    }

    public void setLofk(String lofk) {
        this.lofk = lofk;
    }

    public String getObsData() {
        return obsData;
    }

    public void setObsData(String obsData) {
        this.obsData = obsData;
    }

    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Items{" +
                "name='" + name + '\'' +
                ", lname='" + lname + '\'' +
                ", order='" + order + '\'' +
                ", family='" + family + '\'' +
                ", category='" + category + '\'' +
                ", status='" + status + '\'' +
                ", redBook='" + redbook + '\'' +
                ", lofk='" + lofk + '\'' +
                ", obsData='" + obsData + '\'' +
                ", habitat='" + habitat + '\'' +
                ", population='" + population + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    protected Items(Parcel in) {
        name = in.readString();
        lname = in.readString();
        order = in.readString();
        family = in.readString();
        category = in.readString();
        status = in.readString();
        redbook = in.readString();
        lofk = in.readString();
        obsData = in.readString();
        habitat = in.readString();
        population = in.readString();
        description = in.readString();
        image = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(lname);
        dest.writeString(order);
        dest.writeString(family);
        dest.writeString(category);
        dest.writeString(status);
        dest.writeString(redbook);
        dest.writeString(lofk);
        dest.writeString(obsData);
        dest.writeString(habitat);
        dest.writeString(population);
        dest.writeString(description);
        dest.writeString(image);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Items> CREATOR = new Creator<Items>() {
        @Override
        public Items createFromParcel(Parcel in) {
            return new Items(in);
        }

        @Override
        public Items[] newArray(int size) {
            return new Items[size];
        }
    };
}
