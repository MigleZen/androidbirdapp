package zenkeviciute.migle.bbd.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class MapObservation implements Parcelable {

    private User user;
    private @ServerTimestamp Date timestamp;
    private String name;
    private GeoPoint geo_point;
    private String amount;
    private String description;
    private String image;
    private String observation_id;
    private String date;

    public MapObservation() {

    }

    @Override
    public String toString() {
        return "MapObservation{" +
                "user=" + user +
                ", timestamp=" + timestamp +
                ", name='" + name + '\'' +
                ", geo_point=" + geo_point +
                ", amount='" + amount + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                ", observation_id='" + observation_id + '\'' +
                ", date='" + observation_id + '\'' +
                '}';
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GeoPoint getGeo_point() {
        return geo_point;
    }

    public void setGeo_point(GeoPoint geo_point) {
        this.geo_point = geo_point;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getObservation_id() {
        return observation_id;
    }

    public void setObservation_id(String observation_id) {
        this.observation_id = observation_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    protected MapObservation(Parcel in) {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MapObservation> CREATOR = new Creator<MapObservation>() {
        @Override
        public MapObservation createFromParcel(Parcel in) {
            return new MapObservation(in);
        }

        @Override
        public MapObservation[] newArray(int size) {
            return new MapObservation[size];
        }
    };
}
