package zenkeviciute.migle.bbd.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ObservationItems implements Parcelable {

    private String catalog;
    private String name;
    private String location;
    private String description;
    private String amount;
    private String image;
    private String date;

    public ObservationItems(String catalog, String name, String location, String description, String amount, String image, String date) {
        this.catalog = catalog;
        this.name = name;
        this.location = location;
        this.description = description;
        this.amount = amount;
        this.image = image;
        this.date = date;
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "ObservationItems{" +
                "catalog='" + catalog + '\'' +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", description='" + description + '\'' +
                ", amount='" + amount + '\'' +
                ", image='" + image + '\'' +
                ", date='" + date + '\'' +
                '}';
    }

    protected ObservationItems(Parcel in) {
        catalog = in.readString();
        name = in.readString();
        location = in.readString();
        description = in.readString();
        amount = in.readString();
        image = in.readString();
        date = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(catalog);
        dest.writeString(name);
        dest.writeString(location);
        dest.writeString(description);
        dest.writeString(amount);
        dest.writeString(image);
        dest.writeString(date);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ObservationItems> CREATOR = new Creator<ObservationItems>() {
        @Override
        public ObservationItems createFromParcel(Parcel in) {
            return new ObservationItems(in);
        }

        @Override
        public ObservationItems[] newArray(int size) {
            return new ObservationItems[size];
        }
    };
}
